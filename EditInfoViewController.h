//
//  EditinfoViewController.h
//  DMS Infosystem
//
//  Created by Shreyansh on 19/12/14.
//  Copyright (c) 2014 DMS Infosystem. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EditInfoViewControllerDelegate

-(void)editingInfoWasFinished;

@end


@interface EditInfoViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) id<EditInfoViewControllerDelegate> delegate;

//@property (weak, nonatomic) IBOutlet UITextField *txtFirstname;

//@property (weak, nonatomic) IBOutlet UITextField *txtLastname;

//@property (weak, nonatomic) IBOutlet UITextField *txtAge;

@property (nonatomic) int recordIDToEdit;


- (IBAction)saveInfo:(id)sender;

@end
