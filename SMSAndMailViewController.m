//
//  SMSAndMailViewController.m
//  DMS Infosystem
//
//  Created by Shreyansh on 22/12/14.
//  Copyright (c) 2014 DMS Infosystem. All rights reserved.
//

#import "SMSAndMailViewController.h"

@interface SMSAndMailViewController ()

@end

@implementation SMSAndMailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //  CREATE THE REQUEST
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://59.162.167.52/api/MessageCompose?admin=contact@dmsinfosystem.com&user=amit@dmsinfosystem.com:W124X4&senderID=TEST%20SMS&receipientno=7830268264&msgtxt=This%20is%20a%20test%20SMS%20from%20DMS%20android%20app&state=4"]];
    
    //  CREATE URL CONNECTION AND FIRE REQUEST
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
  //    FIRST TIME IT RECEIVE THE RESPONSE
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    //     WHEN IT RECEIVES THE DATA
}

-(NSCachedURLResponse *)connection:(NSURLConnection*)connection willCachedResponse:(NSCachedURLResponse*)cachedResponse
{
    //  NIL MEANS ITS NOT NECESSARY TO STORE CACHE DATA FOR THIS RESPONSE
    return nil;
}

-(void)connectionDidFinishLoading:(NSURLConnection*)connection
{
    //  WHEN DATA LOADING IS COMPLETELY DONE
}

-(void)connection:(NSURLConnection*)connection didFailWithError:(NSError *)error
{
    //  WHEN THE REQUEST HAS FAILED
}

@end
