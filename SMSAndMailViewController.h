//
//  SMSAndMailViewController.h
//  DMS Infosystem
//
//  Created by Shreyansh on 22/12/14.
//  Copyright (c) 2014 DMS Infosystem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMSAndMailViewController : UIViewController <NSURLConnectionDelegate>
{
    NSMutableArray *_responseData;
}

@end
