//
//  TestViewController.h
//  DMS Infosystem
//
//  Created by Shreyansh on 22/12/14.
//  Copyright (c) 2014 DMS Infosystem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel *recipeLabel;
@property(nonatomic, strong) NSString *recipeName;

@end
