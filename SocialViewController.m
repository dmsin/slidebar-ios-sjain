//
//  SocialViewController.m
//  DMS Infosystem
//
//  Created by Shreyansh on 23/12/14.
//  Copyright (c) 2014 DMS Infosystem. All rights reserved.
//

#import "SocialViewController.h"

@interface SocialViewController ()

@end

@implementation SocialViewController

NSString *socialLink;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    socialLink = [[NSUserDefaults standardUserDefaults] stringForKey:@"socialLinkKey"];
    
    NSURL *myURL = [NSURL URLWithString:socialLink];
    NSURLRequest *myRequest= [NSURLRequest requestWithURL:myURL];
    [socialWebView loadRequest:myRequest];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
