//
//  RecipeBookViewController.m
//  RecipeBook
//
//  Created by Simon Ng on 14/6/12.
//  Copyright (c) 2012 Appcoda. All rights reserved.
//

#import "SearchViewController.h"
#import "FriendsViewController.h"
#import "webDevelopmentViewController.h"
#import "DigitalViewController.h"
#import "DesignViewController.h"
#import "ServicesViewController.h"

@interface RecipeBookViewController ()

@end

@implementation RecipeBookViewController {
    NSArray *recipes;
    NSArray *searchResults;
    NSMutableDictionary *dict;
}

@synthesize tableView = _tableView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSArray *arr1 = [[FriendsViewController alloc]friend];
    NSArray *arr2 = [[webDevelopmentViewController alloc]webd];
    NSArray *arr3 = [[DigitalViewController alloc]digital];
    NSArray *arr4 = [[DesignViewController alloc]design];
    NSArray *arr5 = [[ServicesViewController alloc]service];
    
    recipes = [arr1 arrayByAddingObjectsFromArray:arr2];
    recipes = [recipes arrayByAddingObjectsFromArray:arr3];
    recipes = [recipes arrayByAddingObjectsFromArray:arr4];
    recipes = [recipes arrayByAddingObjectsFromArray:arr5];
    
    dict = [[NSMutableDictionary alloc]init];
    for (int i=0; i<[recipes count]; i++) {
        if(i < [arr1 count])
        {
            [dict setObject:@"WebHostingSubViewController" forKey:[recipes objectAtIndex:i]];
        }
        else if(i < [arr1 count] + [arr2 count])
        {
            [dict setObject:@"webDevelopmentSubViewController" forKey:[recipes objectAtIndex:i]];
        }
        else if(i < [arr1 count] + [arr2 count] + [arr3 count])
        {
            [dict setObject:@"DigitalSubViewController" forKey:[recipes objectAtIndex:i]];
        }
        else if(i < [arr1 count] + [arr2 count] + [arr3 count] + [arr4 count])
        {
            [dict setObject:@"DesignSubViewController" forKey:[recipes objectAtIndex:i]];
        }
        else if(i < [arr1 count] + [arr2 count] + [arr3 count] + [arr4 count] + [arr5 count])
        {
            [dict setObject:@"ServicesSubViewController" forKey:[recipes objectAtIndex:i]];
        }
    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [searchResults count];

    } else {
        return [recipes count];
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"RecipeCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        cell.textLabel.text = [searchResults objectAtIndex:indexPath.row];
    }
    else {
        cell.textLabel.text = [recipes objectAtIndex:indexPath.row];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle: nil];
    UIViewController *vc ;

    if([searchResults count] < 1){
        vc = [mainStoryboard instantiateViewControllerWithIdentifier: [dict objectForKey:[recipes objectAtIndex:[indexPath row]]]];
    }
    else{
        vc = [mainStoryboard instantiateViewControllerWithIdentifier: [dict objectForKey:[searchResults objectAtIndex:[indexPath row]]]];
    }
    [[SlideNavigationController sharedInstance] pushViewController:vc animated:NO];
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate 
                                    predicateWithFormat:@"SELF contains[cd] %@",
                                    searchText];
    
    searchResults = [recipes filteredArrayUsingPredicate:resultPredicate];
}

#pragma mark - UISearchDisplayController delegate methods
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller 
shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString 
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

@end
