//
//  SocialViewController.h
//  DMS Infosystem
//
//  Created by Shreyansh on 23/12/14.
//  Copyright (c) 2014 DMS Infosystem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialViewController : UIViewController
{
    IBOutlet UIWebView *socialWebView;
}

@end
