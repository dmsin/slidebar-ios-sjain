//
//  DBMaintainer.m
//  DMS Infosystem
//
//  Created by Nairitya Khilari on 20/12/14.
//  Simple daatabase creation, insertion and querying.
//  Copyright (c) 2014 DMS Infosystem. All rights reserved.
//

#import "DBMaintainer.h"

static DBMaintainer *sharedInstance = nil;
static sqlite3 *database = nil;
static sqlite3_stmt *statement = nil;

@implementation DBMaintainer

+(DBMaintainer*)getSharedInstannce{
    if (!sharedInstance){
        sharedInstance = [[super allocWithZone:NULL]init];
        [sharedInstance createDB];
    }
    return sharedInstance;
}

-(BOOL)createDB{
    NSString *docsDir;
    NSArray *dirPaths;
    
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    databasePath = [[NSString alloc]initWithString:[docsDir stringByAppendingPathComponent:@"dms.db"]];
    
    BOOL isSuccess = YES;
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if([filemgr fileExistsAtPath:databasePath] == NO){
        const char *dbpath = [databasePath UTF8String];
        
        if(sqlite3_open(dbpath, &database) == SQLITE_OK){
            char *errMsg;
            const char *sql_stmt = "create table if not exists dmsinfo(pId integer primary key, pName text, pPrice long integer)";
            
            if(sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg)){
                isSuccess = NO;
                NSLog(@"Failed to create MOFO Table");
            }
            
            sqlite3_close(database);
            return isSuccess;
        }
        else{
            isSuccess = NO;
            NSLog(@"Failed to open/write databse");
        }
    }
    return isSuccess;
}

- (BOOL) saveData:(NSString *)productCode productPrice:(NSString *)productPrice;
{
    const char *dbpath = [databasePath UTF8String];
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        NSString *insertSQL = [NSString stringWithFormat:@"insert into dmsinfo (pName, pPrice) values (\"%@\", \"%@\")", productCode, productPrice];
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(database, insert_stmt, -1, &statement, NULL);
        
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_reset(statement);
            return YES;
        }
        else
        {
            sqlite3_reset(statement);
            return NO;
        }
        sqlite3_reset(statement);
    }
    return NO;
}

-(NSArray *)findall
{
    const char *dbpath = [databasePath UTF8String];
    if(sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat: @"select pName, pPrice from dmsinfo"];
        const char *query_stmt = [querySQL UTF8String];
        
        NSMutableArray *resultArray = [[NSMutableArray alloc] init];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *pName = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)];
                [resultArray addObject:pName];
                
                NSString *pCode = [[NSString alloc] initWithFormat:@"%s", sqlite3_column_text(statement, 1)];
                [resultArray addObject:pCode];
                
            }
            sqlite3_reset(statement);
            return resultArray;
        }
    }
    return nil;
}

- (BOOL) deleteAnEntry:(NSString *)productCode
{
    const char *dbpath = [databasePath UTF8String];
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        NSString *deleteSQL = [NSString stringWithFormat:@"delete from dmsinfo where pName = \"%@\"", productCode];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(database, delete_stmt, -1, &statement, NULL);
        
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog(@"deleted an entry of %@", productCode);
            sqlite3_reset(statement);
            return YES;
        }
        else
        {
            NSLog(@"Cannot delete the entry of %@", productCode);
            sqlite3_reset(statement);
            return NO;
        }
        //sqlite3_reset(statement);
    }
    return NO;

}

- (BOOL) deleteAll
{
    const char *dbpath = [databasePath UTF8String];
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        NSString *truncateSQL = [NSString stringWithFormat:@"delete from dmsinfo"];
        const char *truncate_stmt = [truncateSQL UTF8String];
        sqlite3_prepare_v2(database, truncate_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog(@"Truncating the table");
            sqlite3_reset(statement);
            return YES;
        }
        else
        {
            NSLog(@"Cannot truncate the table");
            sqlite3_reset(statement);
            return NO;
        }
        //sqlite3_reset(statement);
    }
    return NO;
}

-(BOOL) checkWithName: (NSString *)ProductName
{
    const char *dbpath = [databasePath UTF8String];
    if(sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat: @"select pPrice from dmsinfo where pName = \"%@\"", ProductName];
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                sqlite3_reset(statement);
                return NO;
            }
            sqlite3_reset(statement);
        }
    }
    return YES;
}

@end
