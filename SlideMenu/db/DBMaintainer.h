//
//  DBMaintainer.h
//  DMS Infosystem
//
//  Created by Nairitya Khilari on 20/12/14.
//  Copyright (c) 2014 DMS Infosystem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DBMaintainer : NSObject
{
    NSString *databasePath;
}

+(DBMaintainer *)getSharedInstannce;
-(BOOL)createDB;
-(BOOL) saveData: (NSString *)productCode productPrice:(NSString *)productPrice;
-(NSArray*) findall;
- (BOOL) deleteAnEntry:(NSString *)productCode;
- (BOOL) deleteAll;
-(BOOL) checkWithName: (NSString *)ProductName;
@end
