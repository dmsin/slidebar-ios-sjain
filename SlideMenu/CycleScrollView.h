//
//  CycleScrollView.h
//  CycleScrollDemo'


#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

typedef enum {
    CycleDirectionPortait,CycleDirectionLandscape
}CycleDirection;

@protocol CycleScrollViewDelegate;


@interface CycleScrollView : UIView <UIScrollViewDelegate> {
    
    UIScrollView *scrollView;
    UIImageView *curImageView;
    
    long totalPage;
    int curPage;
    CGRect scrollFrame;
    
    CycleDirection scrollDirection;
    NSArray *imagesArray;
    NSMutableArray *curImages;
    
}

@property (nonatomic, assign) id delegate;
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;

- (int)validPageValue:(int)value;
- (id)initWithFrame:(CGRect)frame cycleDirection:(CycleDirection)direction pictures:(NSArray *)pictureArray;
- (NSArray *)getDisplayImagesWithCurpage:(int)page;
- (void)refreshScrollView;

@end

@protocol CycleScrollViewDelegate <NSObject>
@optional
- (void)cycleScrollViewDelegate:(CycleScrollView *)cycleScrollView didSelectImageView:(int)index;
- (void)cycleScrollViewDelegate:(CycleScrollView *)cycleScrollView didScrollImageView:(int)index;

@end