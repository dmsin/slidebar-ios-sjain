//
//  cartViewController.m
//  SlideMenu
//
//  Created by Shreyansh on 13/12/14.
//  Copyright (c) 2014 Aryan Ghassemi. All rights reserved.
//

#import "cartViewController.h"


@interface cartViewController ()

@end

@implementation cartViewController

//From Parent
NSString *toChildProductCode;
NSString *toChildProductPrice;

NSString *arrPeopleInfo;
//DBManager *dbManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    toChildProductCode = [[NSUserDefaults standardUserDefaults] stringForKey:@"toChildProductCode"];
    toChildProductPrice = [[NSUserDefaults standardUserDefaults] stringForKey:@"toChildProductPrice"];
    NSLog(@"Got the toChildProductCode %@ and toChildProductPrice %@", toChildProductCode, toChildProductPrice);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

@end
