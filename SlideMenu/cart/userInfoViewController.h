//
//  userInfoViewController.h
//  DMS Infosystem
//
//  Created by Nairitya Khilari on 20/12/14.
//  Copyright (c) 2014 DMS Infosystem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface userInfoViewController : UIViewController <NSURLConnectionDelegate>
{
    NSMutableArray *_responseData;
}
@property (strong, nonatomic) IBOutlet UITextField *userName;
@property (strong, nonatomic) IBOutlet UITextField *userPhone;
@property (strong, nonatomic) IBOutlet UITextField *userEmail;
@property (strong, nonatomic) IBOutlet UITextField *userCity;

@property (strong, nonatomic) IBOutlet UIButton *userSubmitButt;
-(IBAction)userSubmitAct:(id)sender;

@end
