//
//  userInfoViewController.m
//  DMS Infosystem
//
//  Created by Nairitya Khilari on 20/12/14.
//  Copyright (c) 2014 DMS Infosystem. All rights reserved.
//

#import "userInfoViewController.h"
#import "DBMaintainer.h"
#import "RightMenuViewController.h"
//#import "MailCore/mailcore.h"

@interface userInfoViewController ()

@end

@implementation userInfoViewController
@synthesize userName;
@synthesize userPhone;
@synthesize userCity;
@synthesize userEmail;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.userSubmitButt.enabled = NO;
    [self.userName addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.userPhone addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.userEmail addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.userCity addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(BOOL) MobileNumberChecker:(NSString *)number{
    NSString *numberRegrex = @"[0-9]{10}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@" , numberRegrex];
    if([numberTest evaluateWithObject:number] == YES)
        return TRUE;
    else
        return FALSE;
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(void) textFieldDidChange:(UITextField *)textField{
    if(userName.text.length > 0 && userPhone.text.length > 0 && userEmail.text.length > 0 && userCity.text.length > 0 && [self MobileNumberChecker:userPhone.text] && [self NSStringIsValidEmail:userEmail.text])
    {
        self.userSubmitButt.enabled = YES;
    }
    else{
        self.userSubmitButt.enabled = NO;
    }
}

-(IBAction)userSubmitAct:(id)sender
{
    NSString *commonMessageData = @"";
    commonMessageData = [[NSString alloc] initWithFormat:@"Thank you %@ , for Placing your Order \r", userName.text];
    NSArray *allDbData;
    allDbData = [[DBMaintainer getSharedInstannce] findall];
    for(int i = 0 ; i < [allDbData count]/2 ; i++)
    {
        commonMessageData = [commonMessageData stringByAppendingFormat:@"%d. %@ - %@ \n" , (i+1) ,allDbData[2*i],allDbData[2*i + 1]];
        commonMessageData = [commonMessageData stringByAppendingString:@"\n"];
    }

    NSLog(@"%@", commonMessageData);
    commonMessageData = [commonMessageData stringByAppendingString:@"\r with DMS InfoSystem. You will receive a call from our executive soon"];
    
//    MCOSMTPSession *smtpSession = [[MCOSMTPSession alloc] init];
//    smtpSession.hostname = @"smtp.gmail.com";
//    smtpSession.port = 465;
//    smtpSession.username = @"marketing@dmsinfosystem.com";
//    smtpSession.password = @"Sm5H=dNX";
//    smtpSession.authType = MCOAuthTypeSASLPlain;
//    smtpSession.connectionType = MCOConnectionTypeTLS;
//    
//    MCOMessageBuilder *builder = [[MCOMessageBuilder alloc] init];
//    MCOAddress *from = [MCOAddress addressWithDisplayName:@"DMS Infosystem" mailbox:@"marketing@dmsinfosystem.com"];
//    MCOAddress *to = [MCOAddress addressWithDisplayName:nil mailbox:userEmail.text];
//    [[builder header] setFrom:from];
//    [[builder header] setTo:@[to]];
//    [[builder header] setSubject:@"DMS InfoSystem Purchase"];
//    [builder setHTMLBody:commonMessageData];
//    NSData * rfc822Data = [builder data];
//    
//    MCOSMTPSendOperation *sendOperation =
//    [smtpSession sendOperationWithData:rfc822Data];
//    [sendOperation start:^(NSError *error) {
//        if(error) {
//            NSLog(@"Error sending email: %@", error);
//        } else {
//            NSLog(@"Successfully sent email!");
//        }
//    }];
    
    
   // NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://59.162.167.52/api/MessageCompose?admin=contact@dmsinfosystem.com&user=amit@dmsinfosystem.com:W124X4&senderID=TEST%20SMS&receipientno=7583837742&msgtxt=This%20is%20a%20test%20SMS%20from%20DMS%20android%20app&state=4"]];
    
    NSString *url = @"http://59.162.167.52/api/MessageCompose?admin=contact@dmsinfosystem.com&user=amit@dmsinfosystem.com:W124X4&senderID=TEST%20SMS&receipientno=";
    url = [url stringByAppendingString:userPhone.text];
    url = [url stringByAppendingString:@"&msgtxt=Thank you"];
    url = [url stringByAppendingString:commonMessageData];
    url = [url stringByAppendingString:@"&state=4"];
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
   
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    NSLog(@"SMS Sent %@", conn);
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [[DBMaintainer getSharedInstannce] deleteAll];
    
    UIAlertView *alertMessage = [[UIAlertView alloc]initWithTitle:@"Request sent !!" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertMessage show];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone"
                                                             bundle: nil];
    
    UIViewController *vc ;
    
    vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"HomeViewController"];
    [[RightMenuViewController alloc] reloadtableData];
    [[SlideNavigationController sharedInstance] pushViewController:vc animated:NO];
    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
}

-(NSCachedURLResponse *)connection:(NSURLConnection*)connection willCachedResponse:(NSCachedURLResponse*)cachedResponse
{
    return nil;
}

-(void)connectionDidFinishLoading:(NSURLConnection*)connection
{
}

-(void)connection:(NSURLConnection*)connection didFailWithError:(NSError *)error
{
}

@end
