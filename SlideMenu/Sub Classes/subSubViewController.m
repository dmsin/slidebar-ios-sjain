//
//  subSubViewController.m
//  SlideMenu
//
//  Created by Nairitya Khilari on 14/12/14.
//

#import "subSubViewController.h"

@interface subSubViewController ()
- (NSInteger)numberOfRows;
@end

@implementation subSubViewController


//Variable from parent
NSString *mainService;
NSString *subService;

//send to child

//Web Hosting - 1. Starter
NSArray *hostingStarter;
//Web Hosting - 2. Silver
NSArray *hostingSilver;
//Web Hosting - 3. Gold
NSArray *hostingGold;
//Web Hosting - 4. Platinum
NSArray *hostingPlatinum;
//Web Hosting - 5. Unlimited
NSArray *hostingUnlimited;


//Reseller Hosting - 1. Starter
NSArray *resellerStarter;
//Reseller Hosting - 2. Silver
NSArray *resellerSilver;
//Reseller Hosting - 3. Gold
NSArray *resellerGold;
//Reseller Hosting - 4. Platinum
NSArray *resellerPlatinum;

//EMail Hosting
NSArray *emailHostingBasic;
NSArray *emailHostingStarndart;
NSArray *emailHostingPremium;
NSArray *emailHostingPremiumPlus;


//Website Development
NSArray *developmentWebsiteBasic;
NSArray *developmentWebsiteStandard;
NSArray *developmentWebsitePremium;
NSArray *developmentWebsitePremiumPlus;


//E-Commerce Development
NSArray *eCommerceBasic;
NSArray *eCommerceStandard;
NSArray *eCommercePremium;
NSArray *eCommercePremiumPlus;


//
NSArray *digitalSEOSilver;
NSArray *digitalSEOGold;
NSArray *digitalSEOPlatinum;
NSArray *digitalSEOUnlimited;

NSArray *digitalSocialMarketingStarter;
NSArray *digitalSocialMarketingSilver;
NSArray *digitalSocialMarketingGold;
NSArray *digitalSocialMarketingPlatinum;


NSArray *designLogoBasic;
NSArray *designLogoStandard;
NSArray *designLogoPremium;
NSArray *designLogoPremiumPlus;

NSArray *designEmailerBasic;
NSArray *designEmailerStandard;
NSArray *designEmailerPremium;
NSArray *designEmailerPremiumPlus;

NSArray *designBrochureBasic;
NSArray *designBrochureStandard;
NSArray *designBrochurePremium;
NSArray *designBrochureCustom;

NSArray *designStationaryBasic;
NSArray *designStationaryStandard;
NSArray *designStationaryPremium;

NSArray *designCustomFb;
NSArray *designCustomTweeter;
NSArray *designCustomGoogle;
NSArray *designCustomWebsite;

//--
NSArray *servicesWebsiteMaintennance;
NSArray *servicesBulkSMS;
NSArray *servicesBulkEmailer;
NSArray *servicesContentWriting;
//--

//Total Number of Rows.
NSInteger totalRows;

//Send This to child -> 1. Product Code
NSString *toChildProductCode;

//Send this to child -> 2. Product Price
NSString *toChildProductPrice;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImageView *imageLogoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DMS LOGO.png"]];
    imageLogoView.frame = CGRectMake(130, 80, 60, 30);
    self.navigationItem.titleView = imageLogoView;
    
    UIButton *cartButton  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [cartButton setImage:[UIImage imageNamed:@"cart"] forState:UIControlStateNormal];
    [cartButton addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleRightMenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cartBarButton = [[UIBarButtonItem alloc] initWithCustomView:cartButton ];
    
    UIButton *searchButton  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [searchButton setImage:[UIImage imageNamed:@"search.gif"] forState:UIControlStateNormal];
    [searchButton addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *searchBarButton = [[UIBarButtonItem alloc] initWithCustomView:searchButton ];
    
    self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects:cartBarButton, searchBarButton, nil];
    
    mainService = [[NSUserDefaults standardUserDefaults] stringForKey:@"mainService"];
    subService = [[NSUserDefaults standardUserDefaults] stringForKey:@"subService"];
    
    
    //Hosting -> 1. Wb Hosting//
    hostingStarter = [[NSArray alloc]initWithObjects:@"100 MB Webspace", @"1024 MB Bandwidth", @"1 Website", @"3 Email IDs", @"2 Sub Domains", @"2 MySQL Space", nil];
    hostingSilver = [[NSArray alloc]initWithObjects:@"1024 MB Webspace", @"2048 MB Bandwidth", @"5 Email IDs", @"3 Sub Domain", @"3 MySQL Space", nil];
    hostingGold = [[NSArray alloc]initWithObjects:@"10240 MB Webspace", @"5120 MB Bandwidth", @"1 Website", @"15 Email IDs", @"5 Sub Domain", @"5 MySQL Space", @"Control Panel", nil];
    hostingPlatinum = [[NSArray alloc]initWithObjects:@"Unlimited Webspace", @"10240 MB Bandwidth", @"1 Website", @"20 Email IDs", @"10 Sub Domain", @"10 MySQL Space", @"Control Panel", nil];
    hostingUnlimited = [[NSArray alloc]initWithObjects:@"Unlimited Webspace", @"20480 MB Bandwidth", @"1 Website", @"Unlimited Email IDs", @"Unlimited Sub Domain", @"Unlimited MySQL Space", @"Control Panel", @"1 Domain Free", nil];
    
    //Hosting -> 2. Reseller Hosting//
    resellerStarter = [[NSArray alloc]initWithObjects:@"25600MB Web Space", @"256000MB Bandwidth", @"25 Account", @"100 Domains", @"Unlimited Email Id", @"Unlimited Sub Domain", @"Control Panel", @"WHM Control", nil];
    resellerSilver = [[NSArray alloc]initWithObjects:@"51200MB Web Space", @"512000MB Bandwidth", @"50 Account", @"200 Domains", @"Unlimited Email Id", @"Unlimited Sub Domain", @"Control Panel", @"WHM Control", nil];
    resellerGold = [[NSArray alloc]initWithObjects:@"100GB Web Space", @"1000GB Bandwidth", @"100 Account", @"300 Domains", @"Unlimited Email Id", @"Unlimited Sub Domain", @"Control Panel", @"WHM Control", nil];
    resellerPlatinum = [[NSArray alloc]initWithObjects:@"200GB Web Space", @"2000GB Bandwidth", @"Unlimited Accounts", @"Unlimited Domains", @"Unlimited Email Id", @"Unlimited Sub Domain", @"Control Panel", @"WHM Control", nil];
    
    //Hosting -> 3. EMail Hosting//
    emailHostingBasic = [[NSArray alloc]initWithObjects:@"100MB Web Space", @"1 Website", @"3 Email Ids", @"2 Sub Domain", @"2 MySQL Database", @"C-Panel", nil];
    emailHostingStarndart = [[NSArray alloc]initWithObjects:@"1024MB Web Space", @"1 Website", @"5 Email Ids", @"3 Sub Domain", @"3 MySQL Database", @"C-Panel", nil];
    emailHostingPremium = [[NSArray alloc]initWithObjects:@"10240MB Web Space", @"1 Website", @"15 Email Ids", @"5 Sub Domain", @"5 MySQL Database", @"C-Panel", nil];
    emailHostingPremiumPlus = [[NSArray alloc]initWithObjects:@"Unlimited Web Space", @"1 Website", @"20 Email Ids", @"10 Sub Domain", @"10 MySQL Database", @"C-Panel", nil];
    
    //Development -> 1. Website Development//
    developmentWebsiteBasic = [[NSArray alloc]initWithObjects:@"100MB Web Space", @"1 Website", @"3 Email IDs", @"2 Sub Domains", @"2 MySQL Database", @"C-Panel", nil];
    developmentWebsiteStandard = [[NSArray alloc]initWithObjects:@"1024MB Web Space", @"1 Website", @"5 Email IDs", @"3 Sub Domains", @"3 MySQL Database", @"C-Panel", nil];
    developmentWebsitePremium = [[NSArray alloc]initWithObjects:@"10240MB Web Space", @"1 Website", @"15 Email IDs", @"5 Sub Domains", @"5 MySQL Database", @"C-Panel", nil];
    developmentWebsitePremiumPlus = [[NSArray alloc]initWithObjects:@"Unlimited Web Space", @"1 Website", @"20 Email IDs", @"10 Sub Domains", @"10 MySQL Database", @"C-Panel", nil];
    
    //Development -> 3. E-COmmerce//
    eCommerceBasic = [[NSArray alloc]initWithObjects:@"2GB Hosting Space", @"1 Domain", @"5 Email IDs", @"P-Control Panel", @"1 Year Online Support", @"INR 19999", nil];
    eCommerceStandard = [[NSArray alloc]initWithObjects:@"10GB Hosting Space", @"1 Domain", @"12 Email IDs", @"25 Pges Max", @"P-Control Panel", @"1 Year Online Support", @"INR 29999", nil];
    eCommercePremium = [[NSArray alloc]initWithObjects:@"20GB Hosting Space", @"1 Domain", @"25 Email IDs", @"50 Pges Max", @"P-Control Panel", @"1 Year Online Support", @"INR 29999", nil];
    eCommercePremiumPlus = [[NSArray alloc]initWithObjects:@"25GB Hosting Space", @"1 Domain", @"50 Email IDs", @"100 Pges Max", @"P-Control Panel", @"1 Year Online Support", @"INR 59999", nil];
    
    
    //Digital -> 1. SEO
    digitalSEOSilver = [[NSArray alloc]initWithObjects:@"Has Advanced Keyword Research", @"5 Keywords to Target", @"5 Optimized URL", @"Has Audit", @"Has Proposal", @"Has Monthly Report", @"Onpage Optimization", nil];
    digitalSEOGold = [[NSArray alloc]initWithObjects:@"Has Advanced Keyword Research", @"8 Keywords to Target", @"8 Optimized URL", @"Has Audit", @"Has Proposal", @"Has Monthly Report", @"Onpage Optimization", nil];
    digitalSEOPlatinum = [[NSArray alloc]initWithObjects:@"Has Advanced Keyword Research", @"10 Keywords to Target", @"10 Optimized URL", @"Has Audit", @"Has Proposal", @"Has Monthly Report", @"Onpage Optimization", nil];
    digitalSEOUnlimited = [[NSArray alloc]initWithObjects:@"Has Advanced Keyword Research", @"12 Keywords to Target", @"12 Optimized URL", @"Has Audit", @"Has Proposal", @"Has Monthly Report", @"Onpage Optimization", nil];
    
    //Digital -> 2. Social Marketing
    digitalSocialMarketingStarter = [[NSArray alloc]initWithObjects:@"LINKEDIN", nil];
    digitalSocialMarketingSilver = [[NSArray alloc]initWithObjects:@"LINKEDIN", nil];
    digitalSocialMarketingPlatinum = [[NSArray alloc]initWithObjects:@"LINKEDIN", nil];
    digitalSocialMarketingGold = [[NSArray alloc]initWithObjects:@"LINKEDIN", nil];
    
    
    //Design -> 1. Logo
    designLogoBasic = [[NSArray alloc]initWithObjects:@"2 Logo Concepts", @"5 Revisions", @"48-72hrs hrs", @"Formats: gif, png, jpg", @"INR 3000", nil];
    designLogoStandard = [[NSArray alloc]initWithObjects:@"3 Logo Concepts", @"10 Revisions", @"48-72hrs hrs", @"Formats: gif, png, jpg", @"INR 5000", nil];
    designLogoPremium = [[NSArray alloc]initWithObjects:@"5 Logo Concepts", @"Unlimited Revisions", @"48-72hrs hrs", @"All with psd", @"With Brand guidelines", @"INR 15000", nil];
    
    //Design -> 2. Emailer ==> No data
    
    //Design -> 3. Brochure
    designBrochureBasic = [[NSArray alloc]initWithObjects:@"4 Number of Pages", @"Custom Design Cover", @"Custom Design Pages", @"2 Design Concept", @"2 Rounds of Revision", @"Add on Photos", @"Client Provided text content", @"Client Provided Graphics", @"INR 2999", nil];
    designBrochureStandard = [[NSArray alloc]initWithObjects:@"1(2 sides) Number of Pages", @"Custom Design Cover", @"Custom Design Pages", @"2 Design Concept", @"2 Rounds of Revision", @"Add on Photos", @"Client Provided text content", @"Client Provided Graphics", @"INR 4999", nil];
    designBrochurePremium = [[NSArray alloc]initWithObjects:@"8 Number of Pages", @"Custom Design Cover", @"Custom Design Pages", @"2 Design Concept", @"3 Rounds of Revision", @"Add on Photos", @"Client Provided text content", @"Client Provided Graphics", @"INR 9999", nil];
    designBrochureCustom = [[NSArray alloc]initWithObjects:@"Varies Number of Pages", @"Custom Design Cover", @"Custom Design Pages", @"Varies Design Concept", @"Varies Rounds of Revision", @"Add on Photos", @"Client Provided text content", @"Client Provided Graphics", @"Varies Price Details", nil];
    
    //Design -> 4. Stationary
    designStationaryBasic = [[NSArray alloc]initWithObjects:@"Business Card Design", @"Letter Head Design", @"Envelop Design", @"INR 3999", nil];
    designStationaryStandard = [[NSArray alloc]initWithObjects:@"Business Card Design", @"Letter Head Design(2 sizes)", @"Envelop Design(2 sizes)", @"Bill Book", @"Payment Voucher", @"Cash Voucher", @"INR 7999", nil];
    designStationaryPremium = [[NSArray alloc]initWithObjects:@"Business Card Design", @"Letter Head Design(2 sizes)", @"Envelop Design(2 sizes)", @"Bill Book", @"Payment Voucher", @"Cash Voucher", @"CD Sticker", @"CD Cover", @"File Folder", @"Book Marks", @"Rough Pad", @"Challan Book", @"Visitor Pass", @"INR 11999", nil];
    
    //Design -> 5. Custom
    designCustomFb = [[NSArray alloc]initWithObjects:@"Attractive Facebook Cover", @"Facebook Profile Logo", @"INR 2000", nil];
    designCustomTweeter = [[NSArray alloc]initWithObjects:@"Twitter Background", @"Twitter Logo", @"INR 2000", nil];
    designCustomGoogle = [[NSArray alloc]initWithObjects:@"6 Static Image Ads", @"6 Flash Ads", @"4 Mobile Ads", @"INR 4999", nil];
    designCustomWebsite = [[NSArray alloc]initWithObjects:@"5 Static Flash Banner", @"2 Display Banner", @"2 Flash", @"INR 4999", nil];
    
    //Services -> 1. Website Maintenance
    servicesWebsiteMaintennance = [[NSArray alloc] initWithObjects:@"Why Website Maintenance?\n\nWebsite Maintenance plays a key role in the success of your online business. It is essential to make your website updated and make to make your website looks professional.\n\n\nWhy Choose Us?\n\nWe keenly monitor your website for uptime, updates and for backups. We are readily accessible to you for solving your queries. We provide bankable hours, online ticketing system, time tracking systems etc. that make us highly accountable for your investment. " ,nil];
    //Services -> 2. Bulk SMS
    servicesBulkSMS = [[NSArray alloc] initWithObjects:@"Why bulk SMS?\n\nBulk SMS uses modern technology to send Bulk SMS via internet to the folks on the handheld device. This mode of communication is the perfect way to pass important messages like launches, promotion etc. of a service or product. It’s also a great way to simultaneously reach a huge group of audience including your clients, staff or channel members.\n\n\nWhy Choose Us?\n\nWe offer SMS services for both Transactional and Informational messages We deliver information to the correct audience at the right time We always stay in touch with our clienteles and provide support to them at every stage", nil];
    //Services -> 3. Bulk Emailer
    servicesBulkEmailer = [[NSArray alloc] initWithObjects:@"Why Bulk Emailer?\n\nBulk E-Mailer service is used reach a great number of recipients through emails. It boosts conversion rates and lifetime client value and makes lucrative relationships with clients and prospects. Our Offerings Strategic Planning Email Distribution Email Design & Production Reporting & Analysis Email Tracking", nil];
    //Services -> 4. Content Writing
    servicesContentWriting = [[NSArray alloc] initWithObjects:@"Why Content Writing is Essential?\n\nContent is considered as the foundation of the company. It should be informative, well-researched, educational and well presented to engage the audience.\n\n\nWhat We Offer?\n\n100% unique content Domain specific content Content written by the professional writers Copyscape verified content Content written by using relevant keywords Contents for Catalogs and Brochures Content with a perfect blend of knowledge and creativity Contents for Basic Articles and Press releases", nil];
    
    
    totalRows = self.numberOfRows;
    NSLog(@"mainService is %@, and subService is %@", mainService, subService);
}

-(void)searchAction
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle: nil];
    UIViewController *vc ;
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"rohan"];
    [[SlideNavigationController sharedInstance] pushViewController:vc animated:NO];
    NSLog(@"search button clicked");
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}


- (NSInteger)numberOfRows
{
    if([mainService isEqualToString:@"Web Hosting"]){
        if([subService isEqualToString:@"Starter"])
            return [hostingStarter count];
        else if([subService isEqualToString:@"Silver"])
            return [hostingSilver count];
        else if([subService isEqualToString:@"Gold"])
            return [hostingGold count];
        else if([subService isEqualToString:@"Platinum"])
            return [hostingPlatinum count];
        else if([subService isEqualToString:@"Unlimited"])
            return [hostingUnlimited count];
    }
    else if([mainService isEqualToString:@"Reseller Hosting"]){
        if([subService isEqualToString:@"Starter"])
            return [resellerStarter count];
        else if([subService isEqualToString:@"Silver"])
            return [resellerSilver count];
        else if([subService isEqualToString:@"Gold"])
            return [resellerGold count];
        else if([subService isEqualToString:@"Platinum"])
            return [resellerPlatinum count];
    }
    else if([mainService isEqualToString:@"Email Hosting"]){
        if([subService isEqualToString:@"Basic"])
            return [emailHostingBasic count];
        else if([subService isEqualToString:@"Standard"])
            return [emailHostingStarndart count];
        else if([subService isEqualToString:@"Premium"])
            return [emailHostingPremium count];
        else if([subService isEqualToString:@"Premium Plus"])
            return [emailHostingPremiumPlus count];
    }
    
    
    
    else if([mainService isEqualToString:@"Website Development"]){
        if([subService isEqualToString:@"Basic"])
            return [developmentWebsiteBasic count];
        else if([subService isEqualToString:@"Standard"])
            return [developmentWebsiteStandard count];
        else if([subService isEqualToString:@"Premium"])
            return [developmentWebsitePremium count];
        else if([subService isEqualToString:@"Premium Plus"])
            return [developmentWebsitePremiumPlus count];
    }
    else if([mainService isEqualToString:@"E-Commerce"]){
        if([subService isEqualToString:@"Basic"])
            return [eCommerceBasic count];
        else if([subService isEqualToString:@"Standard"])
            return [eCommerceStandard count];
        else if([subService isEqualToString:@"Premium"])
            return [eCommercePremium count];
        else if([subService isEqualToString:@"Premium Plus"])
            return [eCommercePremiumPlus count];
    }
    
    
    else if([mainService isEqualToString:@"SEO"]){
        if([subService isEqualToString:@"Silver"])
            return [digitalSEOSilver count];
        else if([subService isEqualToString:@"Gold"])
            return [digitalSEOGold count];
        else if([subService isEqualToString:@"Platinum"])
            return [digitalSEOPlatinum count];
        else if([subService isEqualToString:@"Unlimited"])
            return [digitalSEOUnlimited count];
    }
    else if([mainService isEqualToString:@"Social Marketing"]){
        if([subService isEqualToString:@"Starter"])
            return [digitalSocialMarketingStarter count];
        else if([subService isEqualToString:@"Silver"])
            return [digitalSocialMarketingSilver count];
        else if([subService isEqualToString:@"Gold"])
            return [digitalSocialMarketingGold count];
        else if([subService isEqualToString:@"Platinum"])
            return [digitalSocialMarketingPlatinum count];
    }
    
    else if([mainService isEqualToString:@"LOGO"]){
        if([subService isEqualToString:@"Basic"])
            return [designLogoBasic count];
        else if([subService isEqualToString:@"Standard"])
            return [designLogoStandard count];
        else if([subService isEqualToString:@"Premium"])
            return [designLogoPremium count];
        else if([subService isEqualToString:@"Premium Plus"])
            return [designLogoPremiumPlus count];
    }
    else if([mainService isEqualToString:@"Emailer"]){
        if([subService isEqualToString:@"Basic"])
            return [designEmailerBasic count];
        else if([subService isEqualToString:@"Standard"])
            return [designEmailerStandard count];
        else if([subService isEqualToString:@"Premium"])
            return [designEmailerPremium count];
        else if([subService isEqualToString:@"Premium Plus"])
            return [designEmailerPremiumPlus count];
    }
    else if([mainService isEqualToString:@"Brochure"]){
        if([subService isEqualToString:@"Basic"])
            return [designBrochureBasic count];
        else if([subService isEqualToString:@"Standard"])
            return [designBrochureStandard count];
        else if([subService isEqualToString:@"Premium"])
            return [designBrochurePremium count];
        else if([subService isEqualToString:@"Premium Plus"])
            return [designBrochureCustom count];
    }
    else if([mainService isEqualToString:@"Stationary"]){
        if([subService isEqualToString:@"Basic"])
            return [designStationaryBasic count];
        else if([subService isEqualToString:@"Standard"])
            return [designStationaryStandard count];
        else if([subService isEqualToString:@"Premium"])
            return [designStationaryPremium count];
    }
    else if([mainService isEqualToString:@"Custom"]){
        if([subService isEqualToString:@"FB Cover Designing"])
            return [designCustomFb count];
        else if([subService isEqualToString:@"Twitter Designing"])
            return [designCustomTweeter count];
        else if([subService isEqualToString:@"Google Adwords"])
            return [designCustomGoogle count];
        else if([subService isEqualToString:@"Website Banner"])
            return [designCustomWebsite count];
    }
    //--
    else if([mainService isEqualToString:@"Website Maintenance"])
        return [servicesWebsiteMaintennance count];
    else if([mainService isEqualToString:@"Bulk SMS"])
        return [servicesBulkSMS count];
    else if([mainService isEqualToString:@"Bulk Emailer"])
        return [servicesBulkEmailer count];
    else if([mainService isEqualToString:@"Content Writing"])
        return [servicesContentWriting count];
    //--
    return 0;
}

//Number of rows in tableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return totalRows;
}

//Populating the rows of tableView !!
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"friendCell"];
    
    if([mainService isEqualToString:@"Web Hosting"]){
        if([subService isEqualToString:@"Starter"])
            cell.textLabel.text = [hostingStarter objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Silver"])
            cell.textLabel.text = [hostingSilver objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Gold"])
            cell.textLabel.text = [hostingGold objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Platinum"])
            cell.textLabel.text = [hostingPlatinum objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Unlimited"])
            cell.textLabel.text = [hostingUnlimited objectAtIndex:[indexPath row]];
    }
    else if([mainService isEqualToString:@"Reseller Hosting"]){
        if([subService isEqualToString:@"Starter"])
            cell.textLabel.text = [resellerStarter objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Silver"])
            cell.textLabel.text = [resellerSilver objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Gold"])
            cell.textLabel.text = [resellerGold objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Platinum"])
            cell.textLabel.text = [resellerPlatinum objectAtIndex:[indexPath row]];
    }
    else if([mainService isEqualToString:@"Email Hosting"]){
        if([subService isEqualToString:@"Basic"])
            cell.textLabel.text = [emailHostingBasic objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Standard"])
            cell.textLabel.text = [emailHostingStarndart objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Premium"])
            cell.textLabel.text = [emailHostingPremium objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Premium Plus"])
            cell.textLabel.text = [emailHostingPremiumPlus objectAtIndex:[indexPath row]];
    }
    
    
    else if([mainService isEqualToString:@"Website Development"]){
        if([subService isEqualToString:@"Basic"])
            cell.textLabel.text = [developmentWebsiteBasic objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Standard"])
            cell.textLabel.text = [developmentWebsiteStandard objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Premium"])
            cell.textLabel.text = [developmentWebsitePremium objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Premium Plus"])
            cell.textLabel.text = [developmentWebsitePremiumPlus objectAtIndex:[indexPath row]];
    }
    else if([mainService isEqualToString:@"E-Commerce"]){
        if([subService isEqualToString:@"Basic"])
            cell.textLabel.text = [eCommerceBasic objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Standard"])
            cell.textLabel.text = [eCommerceStandard objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Premium"])
            cell.textLabel.text = [eCommercePremium objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Premium Plus"])
            cell.textLabel.text = [eCommercePremiumPlus objectAtIndex:[indexPath row]];
    }
    
    
    else if([mainService isEqualToString:@"SEO"]){
        if([subService isEqualToString:@"Silver"])
            cell.textLabel.text = [digitalSEOSilver objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Gold"])
            cell.textLabel.text = [digitalSEOGold objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Platinum"])
            cell.textLabel.text = [digitalSEOPlatinum objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Unlimited"])
            cell.textLabel.text = [digitalSEOUnlimited objectAtIndex:[indexPath row]];
    }
    else if([mainService isEqualToString:@"Social Marketing"]){
        if([subService isEqualToString:@"Starter"])
            cell.textLabel.text = [digitalSocialMarketingStarter objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Silver"])
            cell.textLabel.text = [digitalSocialMarketingSilver objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Gold"])
            cell.textLabel.text = [digitalSocialMarketingGold objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Platinum"])
            cell.textLabel.text = [digitalSocialMarketingPlatinum objectAtIndex:[indexPath row]];
    }
    
    else if([mainService isEqualToString:@"LOGO"]){
        if([subService isEqualToString:@"Basic"])
            cell.textLabel.text = [designLogoBasic objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Standard"])
            cell.textLabel.text = [designLogoStandard objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Premium"])
            cell.textLabel.text = [designLogoPremium objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Premium Plus"])
            cell.textLabel.text = [designLogoPremiumPlus objectAtIndex:[indexPath row]];
    }
    
    else if([mainService isEqualToString:@"Emailer"]){
        if([subService isEqualToString:@"Basic"])
            cell.textLabel.text = [designEmailerBasic objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Standard"])
            cell.textLabel.text = [designEmailerStandard objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Premium"])
            cell.textLabel.text = [designEmailerPremium objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Premium Plus"])
            cell.textLabel.text = [designEmailerPremiumPlus objectAtIndex:[indexPath row]];
    }
    
    else if([mainService isEqualToString:@"Brochure"]){
        if([subService isEqualToString:@"Basic"])
            cell.textLabel.text = [designBrochureBasic objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Standard"])
            cell.textLabel.text = [designBrochureStandard objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Premium"])
            cell.textLabel.text = [designBrochurePremium objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Premium Plus"])
            cell.textLabel.text = [designBrochureCustom objectAtIndex:[indexPath row]];
    }
    
    else if([mainService isEqualToString:@"Stationary"]){
        if([subService isEqualToString:@"Basic"])
            cell.textLabel.text = [designStationaryBasic objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Standard"])
            cell.textLabel.text = [designStationaryStandard objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Premium"])
            cell.textLabel.text = [designStationaryPremium objectAtIndex:[indexPath row]];
    }
    
    else if([mainService isEqualToString:@"Custom"]){
        if([subService isEqualToString:@"FB Cover Designing"])
            cell.textLabel.text = [designCustomFb objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Twitter Designing"])
            cell.textLabel.text = [designCustomTweeter objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Google Adwords"])
            cell.textLabel.text = [designCustomGoogle objectAtIndex:[indexPath row]];
        else if([subService isEqualToString:@"Website Banner"])
            cell.textLabel.text = [designCustomWebsite objectAtIndex:[indexPath row]];
    }
    
    //--
    else if([mainService isEqualToString:@"Website Maintenance"])
        cell.textLabel.text = [servicesWebsiteMaintennance objectAtIndex:[indexPath row]];
    else if([mainService isEqualToString:@"Bulk SMS"])
        cell.textLabel.text = [servicesBulkSMS objectAtIndex:[indexPath row]];
    else if([mainService isEqualToString:@"Bulk Emailer"])
        cell.textLabel.text = [servicesBulkEmailer objectAtIndex:[indexPath row]];
    else if([mainService isEqualToString:@"Content Writing"])
        cell.textLabel.text = [servicesContentWriting objectAtIndex:[indexPath row]];
    //--
    
    return cell;
}

@end
