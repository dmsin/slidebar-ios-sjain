//
//  webDevelopmentSubViewController.m
//  SlideMenu
//
//  Created by Nairitya Khilari on 13/12/14.
//

#import "webDevelopmentSubViewController.h"
#import "DBMaintainer.h"
#import "RightMenuViewController.h"

@implementation webDevelopmentSubViewController

NSString *packageName;

//For E-Commenrce
NSArray *eCommerceService;
NSArray *eCommerceServicePrice;

//For Website Development
NSArray *webDevelopmentService;
NSArray *webDevelopmentServicePrice;

//For Website ReDesigning
NSArray *websiteReDesigning;
NSArray *websiteReDesigningPrice;

//Send this to child
NSString *webDevelopmentSubServiceChoosen;

//Get from webDevelopmentController
NSString *webDevelopmentServiceChoosen;

//For holding database queries
NSArray *allDbData;
NSTimer *timer;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    packageName = nil;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadtableData) name:@"refreshWebDevelopmentTable" object:nil];
    
    //Global variables to set !!
    webDevelopmentService = [[NSArray alloc]initWithObjects: @"Basic", @"Standard", @"Premium", @"Premium Plus", nil];
    eCommerceService = [[NSArray alloc]initWithObjects:@"Basic", @"Standard", @"Premium", @"Premium Plus", nil];
    
    //Global variable to Price
    webDevelopmentServicePrice = [[NSArray alloc]initWithObjects: @6999,@8999,@14999,@22999,@39999, nil];
    eCommerceServicePrice = [[NSArray alloc]initWithObjects: @19990,@22990,@39990,@59990, nil];
    websiteReDesigningPrice = [[NSArray alloc]initWithObjects: @0,@0,@0,@0, nil];
    
    UIImageView *imageLogoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DMS LOGO.png"]];
    imageLogoView.frame = CGRectMake(130, 80, 60, 30);
    self.navigationItem.titleView = imageLogoView;
    UIButton *cartButton  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [cartButton setImage:[UIImage imageNamed:@"cart"] forState:UIControlStateNormal];
    [cartButton addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleRightMenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cartBarButton = [[UIBarButtonItem alloc] initWithCustomView:cartButton ];
    
    UIButton *searchButton  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [searchButton setImage:[UIImage imageNamed:@"search.gif"] forState:UIControlStateNormal];
    [searchButton addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *searchBarButton = [[UIBarButtonItem alloc] initWithCustomView:searchButton ];
    self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects:cartBarButton, searchBarButton, nil];

    webDevelopmentServiceChoosen = [[NSUserDefaults standardUserDefaults] stringForKey:@"webDevelopmentServiceChoosen"];
   
    [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(reloadtableData) userInfo:nil repeats:YES];
    [timer fire];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([webDevelopmentServiceChoosen isEqualToString:@"Website Development"]){
        return [webDevelopmentService count];
    }
    else if([webDevelopmentServiceChoosen isEqualToString:@"Website Redesigning"]){
        return 1;
    }
    else if([webDevelopmentServiceChoosen isEqualToString:@"E-Commerce"]){
        return [eCommerceService count];
    }
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"friendCell"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setTag:[indexPath row]];
    [button setFrame:CGRectMake(0, 0, 100, 35)];
    
    if ([webDevelopmentServiceChoosen isEqualToString:@"Website Development"]){
        cell.textLabel.text = [webDevelopmentService objectAtIndex:[indexPath row]];
        button.enabled = [[DBMaintainer getSharedInstannce] checkWithName: [[NSString alloc] initWithFormat:@"%@ - %@", webDevelopmentServiceChoosen, [webDevelopmentService objectAtIndex:[indexPath row]]]];
    }
    else if([webDevelopmentServiceChoosen isEqualToString:@"Website Redesigning"]){
        // To disable accessory button
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        // Two lines to produce multiple lines in a singlr Cell.
        cell.textLabel.numberOfLines=0;
        cell.textLabel.lineBreakMode=UILineBreakModeWordWrap;
        
        cell.textLabel.text =@"Why Website Redesigning?\n\nWebsite redesign enhances the appearance of your site and is very helpful in driving increased quality traffic to your website.\n\n\nWhy Choose Us?\n\n•	Artistic yet technologically advanced redesigning to serve a wide range of your marketing goals\n•	Best CMS along with gripping marketing messages\n•	High quality service at reasonable cost\n•	SEO friendly website design\n•	SMO features to take your website to the buzzing social media";
        //button.enabled = [[DBMaintainer getSharedInstannce] checkWithName: [[NSString alloc] initWithFormat:@"%@ - %@", webDevelopmentServiceChoosen, [websiteReDesigning objectAtIndex:[indexPath row]]]];
    }
    else if([webDevelopmentServiceChoosen isEqualToString:@"E-Commerce"]){
        cell.textLabel.text = [eCommerceService objectAtIndex:[indexPath row]];
        button.enabled = [[DBMaintainer getSharedInstannce] checkWithName: [[NSString alloc] initWithFormat:@"%@ - %@", webDevelopmentServiceChoosen, [eCommerceService objectAtIndex:[indexPath row]]]];
    }
    
    if(button.enabled){
        [button setTitle:@"Add to cart" forState:UIControlStateNormal];
    }
    else{
        [button setTitle:@"Added to cart" forState:UIControlStateNormal];
    }
    
    [button addTarget:self action:@selector(downloadButton:) forControlEvents:UIControlEventTouchUpInside];
    
    if([webDevelopmentServiceChoosen isEqualToString:@"Website Redesigning"]){
    }
    else{
    cell.accessoryView = button;
    }
    
    return cell;
}

-(IBAction)downloadButton:(UIButton *)sender
{
    BOOL success = NO;
    
    if ([webDevelopmentServiceChoosen isEqualToString:@"Website Development"]){
        packageName = [[NSString alloc] initWithFormat:@"%@ - %@", webDevelopmentServiceChoosen, [webDevelopmentService objectAtIndex:(long)((UIButton *)sender).tag]];
        success = [[DBMaintainer getSharedInstannce] saveData:packageName productPrice:[webDevelopmentServicePrice objectAtIndex:(long)((UIButton *)sender).tag]];
    }
    else if([webDevelopmentServiceChoosen isEqualToString:@"Website Redesigning"]){
        packageName = [[NSString alloc] initWithFormat:@"%@ - %@", webDevelopmentServiceChoosen, [websiteReDesigning objectAtIndex:(long)((UIButton *)sender).tag]];
        success = [[DBMaintainer getSharedInstannce] saveData:packageName productPrice:[websiteReDesigningPrice objectAtIndex:(long)((UIButton *)sender).tag]];
    }
    else if([webDevelopmentServiceChoosen isEqualToString:@"E-Commerce"]){
        packageName = [[NSString alloc] initWithFormat:@"%@ - %@", webDevelopmentServiceChoosen, [eCommerceService objectAtIndex:(long)((UIButton *)sender).tag]];
        success = [[DBMaintainer getSharedInstannce] saveData:packageName productPrice:[eCommerceServicePrice objectAtIndex:(long)((UIButton *)sender).tag]];
    }
    
    if(success == YES){
        NSLog(@"Choda hai bhai =D");
    }
    else{
        NSLog(@"nhi Choda :'(");
    }
    
    UIAlertView *alertMessage = [[UIAlertView alloc]initWithTitle:@"Added to cart" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [alertMessage show];
    [sender setTitle:@"Added to cart" forState:UIControlStateNormal];
    sender.enabled = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTable" object:nil];

}

-(void)searchAction
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle: nil];
    UIViewController *vc ;
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"rohan"];
    [[SlideNavigationController sharedInstance] pushViewController:vc animated:NO];
}

//What to open will go here ;)
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([webDevelopmentServiceChoosen isEqualToString:@"Website Development"]){
        webDevelopmentSubServiceChoosen = [webDevelopmentService objectAtIndex:[indexPath row]]; //This will send
    }
    else if([webDevelopmentServiceChoosen isEqualToString:@"Website Redesigning"]){
        webDevelopmentSubServiceChoosen = [websiteReDesigning objectAtIndex:[indexPath row]];
    }
    else if([webDevelopmentServiceChoosen isEqualToString:@"E-Commerce"]){
        webDevelopmentSubServiceChoosen = [eCommerceService objectAtIndex:[indexPath row]];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:webDevelopmentSubServiceChoosen forKey:@"subService"];
    [[NSUserDefaults standardUserDefaults] setObject:webDevelopmentServiceChoosen forKey:@"mainService"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)reloadtableData
{
    allDbData = [[DBMaintainer getSharedInstannce] findall];
    [self.tableView reloadData];
}

@end
