//
//  subSubViewController.h
//  SlideMenu
//
//  Created by Nairitya Khilari on 14/12/14.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface subSubViewController : UITableViewController <SlideNavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate>
{
}
@end