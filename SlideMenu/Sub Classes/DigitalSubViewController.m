//
//  DigitalSubViewController.m
//  SlideMenu
//
//  Created by Nairitya Khilari on 13/12/14.
//

#import "DigitalSubViewController.h"
#import "DBMaintainer.h"
#import "RightMenuViewController.h"

@interface DigitalSubViewController ()

@end

@implementation DigitalSubViewController

NSString *packageName;

//For SEO
NSArray *seoService;
NSArray *seoServicePrice;

//For Social Marketing
NSArray *socialMarketingService;
NSArray *socialMarketingServicePrice;

//Change this variables name
NSString *digitalSubServiceChoosen;

//Get from DigitalViewController
NSString *digitalServiceChoosen;

//For holding database queries
NSArray *allDbData;

NSTimer *timer;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /*Global variables to set !!*/
    packageName=nil;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadtableData) name:@"refreshDigitalTable" object:nil];
    
    seoService = [[NSArray alloc]initWithObjects: @"Silver", @"Gold", @"Platinum", @"Unlimited", nil];
    socialMarketingService = [[NSArray alloc]initWithObjects:@"Starter", @"Silver", @"Gold", @"Platinum", nil];
    
    /*Global variable for Prices*/
    seoServicePrice = [[NSArray alloc]initWithObjects: @4999,@8999,@14999,@19999, nil];
    socialMarketingServicePrice = [[NSArray alloc]initWithObjects: @5999,@11999,@16999,@19999, nil];
    
    UIButton *cartButton  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [cartButton setImage:[UIImage imageNamed:@"cart"] forState:UIControlStateNormal];
    [cartButton addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleRightMenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cartBarButton = [[UIBarButtonItem alloc] initWithCustomView:cartButton ];
    self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects:cartBarButton, nil];
    UIImageView *imageLogoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DMS LOGO.png"]];
    imageLogoView.frame = CGRectMake(130, 80, 60, 30);
    self.navigationItem.titleView = imageLogoView;
    
    UIButton *searchButton  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [searchButton setImage:[UIImage imageNamed:@"search.gif"] forState:UIControlStateNormal];
    [searchButton addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *searchBarButton = [[UIBarButtonItem alloc] initWithCustomView:searchButton ];
    self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects:cartBarButton, searchBarButton, nil];
    
    digitalServiceChoosen = [[NSUserDefaults standardUserDefaults] stringForKey:@"digitalServiceChoosen"];
    
    [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(reloadtableData) userInfo:nil repeats:YES];
    [timer fire];
}

-(void)searchAction
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle: nil];
    UIViewController *vc ;
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"rohan"];
    [[SlideNavigationController sharedInstance] pushViewController:vc animated:NO];
    NSLog(@"search button clicked");
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([digitalServiceChoosen isEqualToString:@"SEO"]){
        return [seoService count];
    }
    else if([digitalServiceChoosen isEqualToString:@"Social Marketing"]){
        return [socialMarketingService count];
    }
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"friendCell"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setTag:[indexPath row]];
    [button setFrame:CGRectMake(0, 0, 100, 35)];
    
    if ([digitalServiceChoosen isEqualToString:@"SEO"]){
        cell.textLabel.text = [seoService objectAtIndex:[indexPath row]];
        
        button.enabled = [[DBMaintainer getSharedInstannce] checkWithName: [[NSString alloc] initWithFormat:@"%@ - %@", digitalServiceChoosen, [seoService objectAtIndex:[indexPath row]]]];
    }
    else if([digitalServiceChoosen isEqualToString:@"Social Marketing"]){
        cell.textLabel.text = [socialMarketingService objectAtIndex:[indexPath row]];
        
        button.enabled = [[DBMaintainer getSharedInstannce] checkWithName: [[NSString alloc] initWithFormat:@"%@ - %@", digitalServiceChoosen, [socialMarketingService objectAtIndex:[indexPath row]]]];
    }
    
    if(button.enabled){
        [button setTitle:@"Add to cart" forState:UIControlStateNormal];
    }
    else{
        [button setTitle:@"Added to cart" forState:UIControlStateNormal];
    }
    
    [button addTarget:self action:@selector(downloadButton:) forControlEvents:UIControlEventTouchUpInside];
    cell.accessoryView = button;
    
    return cell;
}

-(IBAction)downloadButton:(UIButton *)sender
{
    ((UIButton *)sender).titleLabel.text = @"Added to cart";
    
    BOOL success;
    
    if ([digitalServiceChoosen isEqualToString:@"SEO"]){
        packageName = [[NSString alloc] initWithFormat:@"%@ - %@", digitalServiceChoosen, [seoService objectAtIndex:(long)((UIButton *)sender).tag]];
        success = [[DBMaintainer getSharedInstannce] saveData:packageName productPrice:[seoServicePrice objectAtIndex:(long)((UIButton *)sender).tag]];
    }
    else if([digitalServiceChoosen isEqualToString:@"Social Marketing"]){
        packageName = [[NSString alloc] initWithFormat:@"%@ - %@", digitalServiceChoosen, [socialMarketingService objectAtIndex:(long)((UIButton *)sender).tag]];
        success = [[DBMaintainer getSharedInstannce] saveData:packageName productPrice:[socialMarketingServicePrice objectAtIndex:(long)((UIButton *)sender).tag]];
        NSLog(@"Social Marketing Package Price: %@",[socialMarketingServicePrice objectAtIndex:(long)((UIButton *)sender).tag]);
    }
    else
    {
        //Removes logical error.
        success = NO;
    }
    
    if(success == YES){
        NSLog(@"Choda hai bhai =D");
    }
    else{
        NSLog(@"nhi Choda :'(");
    }
    
    UIAlertView *alertMessage = [[UIAlertView alloc]initWithTitle:@"Added to cart" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertMessage show];
    [sender setTitle:@"Added to cart" forState:UIControlStateNormal];
    sender.enabled = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTable" object:nil];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([digitalServiceChoosen isEqualToString:@"SEO"]){
        digitalSubServiceChoosen = [seoService objectAtIndex:[indexPath row]]; //This will send
    }
    else if([digitalServiceChoosen isEqualToString:@"Social Marketing"]){
        digitalSubServiceChoosen = [socialMarketingService objectAtIndex:[indexPath row]];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:digitalSubServiceChoosen forKey:@"subService"];
    [[NSUserDefaults standardUserDefaults] setObject:digitalServiceChoosen forKey:@"mainService"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)reloadtableData
{
    allDbData = [[DBMaintainer getSharedInstannce] findall];
    [self.tableView reloadData];
}

@end

