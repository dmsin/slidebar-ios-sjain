//
//  WebHostingSubViewController.h
//  SlideMenu
//
//  Created by Nairitya Khilari on 13/12/14.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface WebHostingSubViewController : UITableViewController <SlideNavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate>
- (void)reloadtableData;
@end