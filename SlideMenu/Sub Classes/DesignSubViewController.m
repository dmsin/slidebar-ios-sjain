//
//  DesignSubViewController.m
//  SlideMenu
//
//  Created by Nairitya on 13/12/14.
//

#import "DesignSubViewController.h"
#import "DBMaintainer.h"
#import "RightMenuViewController.h"

@implementation DesignSubViewController

NSString *packageName;

//For Logo
NSArray *designLogo;
NSArray *designLogoPrice;

//For Buttn Memory
NSArray *bDesign;


//For Emailer
NSArray *designEmailer;
NSArray *designEmailerPrice;

//For Brochure
NSArray *designBrochure;
NSArray *designBrochurePrice;

//For Stationary
NSArray *designStationary;
NSArray *designStationaryPrice;

//For Custom
NSArray *designCustom;
NSArray *designCustomPrice;

//send this to child
NSString *designSubServiceChoosen;

//Get from designViewController
NSString *designServiceChoosen;

//For holding database queries
NSArray *allDbData;

NSTimer *timer;

- (void)viewDidLoad
{
    [super viewDidLoad];
    packageName=nil;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadtableData) name:@"refreshDesignTable" object:nil];
    
    //Global variables to set !!
    designLogo = [[NSArray alloc]initWithObjects: @"Basic", @"Standard", @"Premium", nil];
    designEmailer = [[NSArray alloc] initWithObjects:@"Why e-Mailer Designing?\n\nE-mailers are powerful and cost-effective tools to promote your services/products through a generic or specific message to a target audience. It builds your garner quality leads to push your sales and the revenue while also positively impacting your brand equity.\n\n\nWhy Us?\n\n•	To get aesthetically appealing yet functional designs\n•	To get designs that follow your brand guidelines\n•	To  get your message portrayed and delivered most effectively", nil];
    designBrochure = [[NSArray alloc]initWithObjects:@"Basic", @"Standard", @"Premium", @"Premium Plus", nil];
    designCustom = [[NSArray alloc]initWithObjects:@"FB Cover Designing", @"Twitter Designing", @"Google Adwords", @"Website Banner", nil];
    designStationary = [[NSArray alloc]initWithObjects:@"Basic", @"Standard", @"Premium", nil];
    
    //Global variable for Prices
    designLogoPrice = [[NSArray alloc]initWithObjects: @3000,@5000,@15000, nil];
    designEmailerPrice = [[NSArray alloc]initWithObjects:@0,@0,@0,@0, nil];
    designBrochurePrice = [[NSArray alloc]initWithObjects:@2999,@4999,@9999,@0, nil];
    designCustomPrice = [[NSArray alloc]initWithObjects:@2000,@2000,@4999,@4999, nil];
    designStationaryPrice = [[NSArray alloc]initWithObjects:@3999,@7999,@11999, nil];
    
    UIButton *cartButton  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [cartButton setImage:[UIImage imageNamed:@"cart"] forState:UIControlStateNormal];
    [cartButton addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleRightMenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cartBarButton = [[UIBarButtonItem alloc] initWithCustomView:cartButton ];
    self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects:cartBarButton, nil];
    
    UIImageView *imageLogoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DMS LOGO.png"]];
    imageLogoView.frame = CGRectMake(130, 80, 60, 30);
    self.navigationItem.titleView = imageLogoView;
    
    UIButton *searchButton  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [searchButton setImage:[UIImage imageNamed:@"search.gif"] forState:UIControlStateNormal];
    [searchButton addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *searchBarButton = [[UIBarButtonItem alloc] initWithCustomView:searchButton ];
    self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects:cartBarButton, searchBarButton, nil];

    designServiceChoosen = [[NSUserDefaults standardUserDefaults] stringForKey:@"designServiceChoosen"];
    
    [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(reloadtableData) userInfo:nil repeats:YES];
    [timer fire];
}

-(void)searchAction
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle: nil];
    UIViewController *vc ;
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"rohan"];
    [[SlideNavigationController sharedInstance] pushViewController:vc animated:NO];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([designServiceChoosen isEqualToString:@"LOGO"]){
        return [designLogo count];
    }
    else if([designServiceChoosen isEqualToString:@"Emailer"]){
        return [designEmailer count];
    }
    else if([designServiceChoosen isEqualToString:@"Brochure"]){
        return [designBrochure count];
    }
    else if([designServiceChoosen isEqualToString:@"Stationary"]){
        return [designStationary count];
    }
    else if([designServiceChoosen isEqualToString:@"Custom"]){
        return [designCustom count];
    }
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"friendCell"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    [button setTag:[indexPath row]];
    [button setFrame:CGRectMake(0, 0, 100, 35)];
    
    if ([designServiceChoosen isEqualToString:@"LOGO"]){
        cell.textLabel.text = [designLogo objectAtIndex:[indexPath row]];
        button.enabled = [[DBMaintainer getSharedInstannce] checkWithName: [[NSString alloc] initWithFormat:@"%@ - %@", designServiceChoosen, [designLogo objectAtIndex:[indexPath row]]]];
    }
    else if([designServiceChoosen isEqualToString:@"Emailer"]){
        // To disable accessory button
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        // Two lines to produce multiple lines in a singlr Cell.
        cell.textLabel.numberOfLines=0;
        cell.textLabel.lineBreakMode=UILineBreakModeWordWrap;
        
        cell.textLabel.text = @"Why e-Mailer Designing?\n\nE-mailers are powerful and cost-effective tools to promote your services/products through a generic or specific message to a target audience. It builds your garner quality leads to push your sales and the revenue while also positively impacting your brand equity.\n\n\nWhy Us?\n\n•	To get aesthetically appealing yet functional designs\n•	To get designs that follow your brand guidelines\n•	To  get your message portrayed and delivered most effectively";
        //button.enabled = [[DBMaintainer getSharedInstannce] checkWithName: [[NSString alloc] initWithFormat:@"%@ - %@", designServiceChoosen, [designEmailer objectAtIndex:[indexPath row]]]];
    }
    else if([designServiceChoosen isEqualToString:@"Brochure"]){
        cell.textLabel.text = [designBrochure objectAtIndex:[indexPath row]];
        button.enabled = [[DBMaintainer getSharedInstannce] checkWithName: [[NSString alloc] initWithFormat:@"%@ - %@", designServiceChoosen, [designBrochure objectAtIndex:[indexPath row]]]];
    }
    else if([designServiceChoosen isEqualToString:@"Stationary"]){
        cell.textLabel.text = [designStationary objectAtIndex:[indexPath row]];
        button.enabled = [[DBMaintainer getSharedInstannce] checkWithName: [[NSString alloc] initWithFormat:@"%@ - %@", designServiceChoosen, [designStationary objectAtIndex:[indexPath row]]]];
    }
    else if([designServiceChoosen isEqualToString:@"Custom"]){
        cell.textLabel.text = [designCustom objectAtIndex:[indexPath row]];
        button.enabled = [[DBMaintainer getSharedInstannce] checkWithName: [[NSString alloc] initWithFormat:@"%@ - %@", designServiceChoosen, [designCustom objectAtIndex:[indexPath row]]]];
    }
    
    if(button.enabled){
        [button setTitle:@"Add to cart" forState:UIControlStateNormal];
    }
    else{
        [button setTitle:@"Added to cart" forState:UIControlStateNormal];
    }
    [button addTarget:self action:@selector(downloadButton:) forControlEvents:UIControlEventTouchUpInside];
    if([designServiceChoosen isEqualToString:@"Emailer"]){
    }
    else{
        cell.accessoryView = button;
    }
    
    return cell;
}

-(IBAction)downloadButton:(UIButton *)sender
{
    BOOL success;
    
    if ([designServiceChoosen isEqualToString:@"LOGO"]){
        packageName = [[NSString alloc] initWithFormat:@"%@ - %@", designServiceChoosen,[designLogo objectAtIndex:(long)((UIButton *)sender).tag]];
        success = [[DBMaintainer getSharedInstannce] saveData:packageName productPrice:[designLogoPrice objectAtIndex:(long)((UIButton *)sender).tag]];
    }
    else if([designServiceChoosen isEqualToString:@"Emailer"]){
        packageName = [[NSString alloc] initWithFormat:@"%@ - %@", designServiceChoosen,[designEmailer objectAtIndex:(long)((UIButton *)sender).tag]];
        success = [[DBMaintainer getSharedInstannce] saveData:packageName productPrice:[designEmailerPrice objectAtIndex:(long)((UIButton *)sender).tag]];
    }
    else if([designServiceChoosen isEqualToString:@"Brochure"]){
        packageName = [[NSString alloc] initWithFormat:@"%@ - %@", designServiceChoosen,[designBrochure objectAtIndex:(long)((UIButton *)sender).tag]];
        success = [[DBMaintainer getSharedInstannce] saveData:packageName productPrice:[designBrochurePrice objectAtIndex:(long)((UIButton *)sender).tag]];
    }
    else if([designServiceChoosen isEqualToString:@"Stationary"]){
        packageName = [[NSString alloc] initWithFormat:@"%@ - %@", designServiceChoosen,[designStationary objectAtIndex:(long)((UIButton *)sender).tag]];
        success = [[DBMaintainer getSharedInstannce] saveData:packageName productPrice:[designStationaryPrice objectAtIndex:(long)((UIButton *)sender).tag]];
    }
    else if([designServiceChoosen isEqualToString:@"Custom"]){
        packageName = [[NSString alloc] initWithFormat:@"%@ - %@", designServiceChoosen,[designCustom objectAtIndex:(long)((UIButton *)sender).tag]];
        success = [[DBMaintainer getSharedInstannce] saveData:packageName productPrice:[designCustomPrice objectAtIndex:(long)((UIButton *)sender).tag]];
    }
    else{
        //Removes logical error.
        success = NO;
    }
    
    if(success == YES){
        NSLog(@"Choda hai bhai =D");
    }
    else{
        NSLog(@"nhi Choda :'(");
    }
    
    UIAlertView *alertMessage = [[UIAlertView alloc]initWithTitle:@"Added to cart" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertMessage show];
    [sender setTitle:@"Added to cart" forState:UIControlStateNormal];
    sender.enabled = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTable" object:nil];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([designServiceChoosen isEqualToString:@"LOGO"]){
        designSubServiceChoosen = [designLogo objectAtIndex:[indexPath row]]; //This will send
    }
    else if([designServiceChoosen isEqualToString:@"Emailer"]){
        designSubServiceChoosen = [designEmailer objectAtIndex:[indexPath row]];
    }
    else if([designServiceChoosen isEqualToString:@"Brochure"]){
        designSubServiceChoosen = [designBrochure objectAtIndex:[indexPath row]];
    }
    else if([designServiceChoosen isEqualToString:@"Stationary"]){
        designSubServiceChoosen = [designStationary objectAtIndex:[indexPath row]];
    }
    else if([designServiceChoosen isEqualToString:@"Custom"]){
        designSubServiceChoosen = [designCustom objectAtIndex:[indexPath row]];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:designSubServiceChoosen forKey:@"subService"];
    [[NSUserDefaults standardUserDefaults] setObject:designServiceChoosen forKey:@"mainService"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (void)reloadtableData
{
    allDbData = [[DBMaintainer getSharedInstannce] findall];
    [self.tableView reloadData];
}

@end