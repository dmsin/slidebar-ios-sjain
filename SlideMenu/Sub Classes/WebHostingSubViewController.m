//
//  WebHostingSubViewController.m
//  SlideMenu
//
//  Created by Nairitya Khilari on 13/12/14.
//

#import "WebHostingSubViewController.h"
#import "DBMaintainer.h"
#import "RightMenuViewController.h"

@implementation WebHostingSubViewController

NSString *packageName;

//For Web Hosting
NSArray *webHostingService;
NSArray *webHostingServicePrice;

//For Reseller Hosting
NSArray *resellerHostingService;
NSArray *resellerHostingServicePrice;

//For Email Hosting
NSArray *emailHostingService;
NSArray *emailHostingServicePrice;

//send this to child
NSString *mainService; // <- As a main service <<-- We don't need this, this == webHostingServiceChoosen
NSString *webHostingSubServiceChoosen; // <- As a subservice


//Get from FriendController
NSString *webHostingServiceChoosen;

//For holding database queries
NSArray *allDbData;

UIButton *downloadButton;

NSTimer *timer;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    packageName = nil;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadtableData) name:@"refreshWebHostingTable" object:nil];
    
    /*Global variables to set !!*/
    webHostingService = [[NSArray alloc]initWithObjects: @"Starter", @"Silver", @"Gold", @"Platinum", nil];
    resellerHostingService = [[NSArray alloc]initWithObjects:@"Starter", @"Silver", @"Gold", @"Platinum", nil];
    emailHostingService = [[NSArray alloc]initWithObjects:@"Basic", @"Standard", @"Premium", @"Premium Plus", nil];
    
    /*Global variable to price*/
    webHostingServicePrice = [[NSArray alloc]initWithObjects: @999,@1499,@2999,@4999, nil];
    resellerHostingServicePrice = [[NSArray alloc]initWithObjects: @7999,@14999,@25999,@32999, nil];
    emailHostingServicePrice = [[NSArray alloc]initWithObjects: @1499,@3999,@9999,@15999, nil];
    
    UIButton *cartButton  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [cartButton setImage:[UIImage imageNamed:@"cart"] forState:UIControlStateNormal];
    [cartButton addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleRightMenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cartBarButton = [[UIBarButtonItem alloc] initWithCustomView:cartButton ];
    self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects:cartBarButton, nil];
    UIImageView *imageLogoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DMS LOGO.png"]];
    imageLogoView.frame = CGRectMake(130, 80, 60, 30);
    self.navigationItem.titleView = imageLogoView;
    
    UIButton *searchButton  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [searchButton setImage:[UIImage imageNamed:@"search.gif"] forState:UIControlStateNormal];
    [searchButton addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *searchBarButton = [[UIBarButtonItem alloc] initWithCustomView:searchButton ];
    self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects:cartBarButton, searchBarButton, nil];
    
    webHostingServiceChoosen = [[NSUserDefaults standardUserDefaults] stringForKey:@"webHostingServiceChoosen"];

    UIButton *button  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [button setImage:[UIImage imageNamed:@"cart"] forState:UIControlStateNormal];
    [button addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleRightMenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    [SlideNavigationController sharedInstance].rightBarButtonItem = rightBarButtonItem;
    
    [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(reloadtableData) userInfo:nil repeats:YES];
    [timer fire];
    
}

-(void)searchAction
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle: nil];
    UIViewController *vc ;
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"rohan"];
    [[SlideNavigationController sharedInstance] pushViewController:vc animated:NO];
    NSLog(@"search button clicked");
}


- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([webHostingServiceChoosen isEqualToString:@"Web Hosting"]){
        return [webHostingService count];
    }
    else if([webHostingServiceChoosen isEqualToString:@"Reseller Hosting"]){
        return [resellerHostingService count];
    }
    else if([webHostingServiceChoosen isEqualToString:@"Email Hosting"]){
        return [emailHostingService count];
    }
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"friendCell"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setTag:[indexPath row]];
    [button setFrame:CGRectMake(0, 0, 100, 35)];
    
    if ([webHostingServiceChoosen isEqualToString:@"Web Hosting"]){
        cell.textLabel.text = [webHostingService objectAtIndex:[indexPath row]];
        
        //  CHECKS IF THE PRODUCT IS ALREADY ADDED IN CART OR NOT, HENCE CONTROLS BUTTONS ACCORDINGLY
        button.enabled = [[DBMaintainer getSharedInstannce] checkWithName: [[NSString alloc] initWithFormat:@"%@ - %@", webHostingServiceChoosen, [webHostingService objectAtIndex:[indexPath row]]]];
    }
    else if([webHostingServiceChoosen isEqualToString:@"Reseller Hosting"]){
        cell.textLabel.text = [resellerHostingService objectAtIndex:[indexPath row]];
        button.enabled = [[DBMaintainer getSharedInstannce] checkWithName: [[NSString alloc] initWithFormat:@"%@ - %@", webHostingServiceChoosen, [resellerHostingService objectAtIndex:[indexPath row]]]];
    }
    else if([webHostingServiceChoosen isEqualToString:@"Email Hosting"]){
        cell.textLabel.text = [emailHostingService objectAtIndex:[indexPath row]];
        button.enabled = [[DBMaintainer getSharedInstannce] checkWithName: [[NSString alloc] initWithFormat:@"%@ - %@", webHostingServiceChoosen, [emailHostingService objectAtIndex:[indexPath row]]]];
    }
    
    if(button.enabled){
        [button setTitle:@"Add to cart" forState:UIControlStateNormal];
    }
    else{
        [button setTitle:@"Added to cart" forState:UIControlStateNormal];
    }
    
    [button addTarget:self action:@selector(downloadButton:) forControlEvents:UIControlEventTouchUpInside];
    cell.accessoryView = button;
    
    return cell;
}

-(IBAction)downloadButton:(UIButton *)sender
{
    BOOL success;
    
    UIAlertView *alertMessage = [[UIAlertView alloc]initWithTitle:@"Added to cart" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    if ([webHostingServiceChoosen isEqualToString:@"Web Hosting"]){

        packageName = [[NSString alloc] initWithFormat:@"%@ - %@", webHostingServiceChoosen, [webHostingService objectAtIndex:(long)((UIButton *)sender).tag]];
        success = [[DBMaintainer getSharedInstannce] saveData:packageName productPrice:[webHostingServicePrice objectAtIndex:(long)((UIButton *)sender).tag]];
    }
    else if([webHostingServiceChoosen isEqualToString:@"Reseller Hosting"]){
        packageName = [[NSString alloc] initWithFormat:@"%@ - %@", webHostingServiceChoosen, [resellerHostingService objectAtIndex:(long)((UIButton *)sender).tag]];
        success = [[DBMaintainer getSharedInstannce] saveData:packageName productPrice:[resellerHostingServicePrice objectAtIndex:(long)((UIButton *)sender).tag]];
        
    }
    else if([webHostingServiceChoosen isEqualToString:@"Email Hosting"]){
        packageName = [[NSString alloc] initWithFormat:@"%@ - %@", webHostingServiceChoosen, [emailHostingService objectAtIndex:(long)((UIButton *)sender).tag]];
        success = [[DBMaintainer getSharedInstannce] saveData:packageName productPrice:[emailHostingServicePrice objectAtIndex:(long)((UIButton *)sender).tag]];
    }
    else{
        //Removes logical error.
        success = NO;
    }
    
    if(success == YES){
        NSLog(@"Choda hai bhai =D");
        [alertMessage show];
        
    }
    else{
        NSLog(@"nhi Choda :'(");
    }
    
    [alertMessage show];
    [sender setTitle:@"Added to cart" forState:UIControlStateNormal];
    sender.enabled = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTable" object:nil];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([webHostingServiceChoosen isEqualToString:@"Web Hosting"]){
        webHostingSubServiceChoosen = [webHostingService objectAtIndex:[indexPath row]]; //This will send
    }
    else if([webHostingServiceChoosen isEqualToString:@"Reseller Hosting"]){
        webHostingSubServiceChoosen = [resellerHostingService objectAtIndex:[indexPath row]];
    }
    else if([webHostingServiceChoosen isEqualToString:@"Email Hosting"]){
        webHostingSubServiceChoosen = [emailHostingService objectAtIndex:[indexPath row]];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:webHostingSubServiceChoosen forKey:@"subService"];
    [[NSUserDefaults standardUserDefaults] setObject:webHostingServiceChoosen forKey:@"mainService"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)reloadtableData
{
    allDbData = [[DBMaintainer getSharedInstannce] findall];
    [self.tableView reloadData];}

@end

