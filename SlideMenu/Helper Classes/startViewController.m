//
//  startViewController.m
//  SlideMenu
//
//  Created by Nairitya Khilari on 11/12/14.
//

#import "startViewController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"

@interface startViewController ()
-(void)viewDidLoad;
- (void)didReceiveMemoryWarning;
- (void) sendToHomeController;
@end

@implementation startViewController

int timeToStart = 0;
NSTimer *timer;

- (void)viewDidLoad {
    
    //self.slideOutAnimationEnabled = YES;
    [super viewDidLoad];
    
    UIImageView *imageLogoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DMS LOGO.png"]];
    imageLogoView.frame = CGRectMake(130, 80, 60, 30);
    self.navigationItem.titleView = imageLogoView;
    // Do any additional setup after loading the view.
    
    
    image.animationImages = [NSArray arrayWithObjects:
                             [UIImage imageNamed:@"1.png"],
                             [UIImage imageNamed:@"2.png"],
                             [UIImage imageNamed:@"3.png"],
                             [UIImage imageNamed:@"4.png"],
                             [UIImage imageNamed:@"5.png"],
                             [UIImage imageNamed:@"6.png"],
                             [UIImage imageNamed:@"7.png"],
                             [UIImage imageNamed:@"8.png"],
                             [UIImage imageNamed:@"9.png"],
                             [UIImage imageNamed:@"10.png"],
                             [UIImage imageNamed:@"11.png"],
                             [UIImage imageNamed:@"12.png"],
                             [UIImage imageNamed:@"13.png"],
                             [UIImage imageNamed:@"14.png"],
                             [UIImage imageNamed:@"15.png"],
                             [UIImage imageNamed:@"16.png"],
                             [UIImage imageNamed:@"17.png"],
                             [UIImage imageNamed:@"18.png"],
                             [UIImage imageNamed:@"19.png"],
                             [UIImage imageNamed:@"20.png"],
                             [UIImage imageNamed:@"21.png"],
                             [UIImage imageNamed:@"22.png"],
                             [UIImage imageNamed:@"23.png"],
                             [UIImage imageNamed:@"24.png"],nil];
    [image setAnimationRepeatCount: -1];
    image.animationDuration = 1;
    [image startAnimating];
    
    
    
    timer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(sendToHomeController) userInfo:nil repeats:YES];
    
    if (timeToStart >= 2){
        NSLog(@"Invalidating the timer");
        [timer invalidate];
    }
    else{
        [timer fire];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)startAction:(id)sender {
    
}

- (void) sendToHomeController
{
    if(timeToStart == 1){
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone"
                                                                 bundle: nil];
        UIViewController *vc ;
        vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"HomeViewController"];
        
        //[[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc withSlideOutAnimation:self.slideOutAnimationEnabled andCompletion:nil];
        [[SlideNavigationController sharedInstance] pushViewController:vc animated:NO];
    }
    
    NSLog(@"Opening thingy !! with counter %d", timeToStart);
    timeToStart += 1;
    
    if(timeToStart > 1){
        NSLog(@"Invalidating the timer");
        [timer invalidate];
    }
    
    

}
@end
