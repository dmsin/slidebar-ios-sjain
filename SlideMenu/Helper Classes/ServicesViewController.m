//
//  ServicesViewController.m
//  SlideMenu
//
//  Created by Nairitya Khilari on 12/12/14.
//

#import "ServicesViewController.h"
#import "SearchViewController.h"
#import "DBMaintainer.h"

@implementation ServicesViewController

NSString *serviceServiceChoosen;

NSArray *servicePrice;

NSArray *allDbData;

- (void)viewDidLoad
{
    [super viewDidLoad];
    serviceServices = [self service];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadtableData) name:@"refreshServicesTable" object:nil];
    
    UIButton *cartButton  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [cartButton setImage:[UIImage imageNamed:@"cart"] forState:UIControlStateNormal];
    [cartButton addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleRightMenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cartBarButton = [[UIBarButtonItem alloc] initWithCustomView:cartButton ];
    
    UIButton *searchButton  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [searchButton setImage:[UIImage imageNamed:@"search.gif"] forState:UIControlStateNormal];
    [searchButton addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *searchBarButton = [[UIBarButtonItem alloc] initWithCustomView:searchButton ];
    
    self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects:cartBarButton, searchBarButton, nil];
    
    UIImageView *imageLogoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DMS LOGO.png"]];
    imageLogoView.frame = CGRectMake(130, 80, 60, 30);
    self.navigationItem.titleView = imageLogoView;
    
    servicePrice = [[NSArray alloc] initWithObjects:@0, @0, @0, @0, nil];
}

-(void)searchAction
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle: nil];
    UIViewController *vc ;
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"rohan"];
    [[SlideNavigationController sharedInstance] pushViewController:vc animated:NO];
    NSLog(@"search button clicked");
}

-(void)saveAction
{
    NSLog(@"save button clicked");
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [serviceServices count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"friendCell"];
    
    cell.textLabel.text = [serviceServices objectAtIndex:[indexPath row]];
    cell.textLabel.textColor = [UIColor colorWithRed:17/255 green:112/255 blue:1/255 alpha:1];
    cell.backgroundColor = [UIColor clearColor];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setTitle:@"Add to cart" forState:UIControlStateNormal];
    [button setTag:[indexPath row]];
    [button setFrame:CGRectMake(0, 0, 100, 35)];
    [button addTarget:self action:@selector(downloadButton:) forControlEvents:UIControlEventTouchUpInside];
    //cell.accessoryView = button;

    return cell;
}

-(IBAction)downloadButton:(UIButton *)sender
{
    NSLog(@"Is %@ and %ld", ((UIButton *)sender).titleLabel.text, (long)((UIButton *)sender).tag);
    ((UIButton *)sender).titleLabel.text = @"Added to cart";
    BOOL success;
    success = [[DBMaintainer getSharedInstannce] saveData:[serviceServices objectAtIndex:(long)((UIButton *)sender).tag] productPrice:[servicePrice objectAtIndex:(long)((UIButton *)sender).tag]];
    
    UIAlertView *alertMessage = [[UIAlertView alloc]initWithTitle:@"Added to cart" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertMessage show];
    [sender setTitle:@"Added to cart" forState:UIControlStateNormal];
    sender.enabled = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTable" object:nil];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    serviceServiceChoosen = [serviceServices objectAtIndex:[indexPath row]];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"rohan" forKey:@"subService"];
    [[NSUserDefaults standardUserDefaults] setObject:serviceServiceChoosen forKey:@"mainService"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSArray *)service
{
    return [[NSArray alloc]initWithObjects: @"Website Maintenance", @"Bulk SMS", @"Bulk Emailer", @"Content Writing", nil];
}

- (void)reloadtableData
{
    NSLog(@"Reloading the Table !!");
    allDbData = [[DBMaintainer getSharedInstannce] findall];
    [self.tableView reloadData];
}
@end

