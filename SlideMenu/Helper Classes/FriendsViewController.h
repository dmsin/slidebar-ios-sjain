//
//  FriendsViewController.h
//  SlideMenu
//
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface FriendsViewController : UITableViewController <SlideNavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate>
{
    NSArray *hosting_servies;
}
- (NSArray *)friend;
@end
