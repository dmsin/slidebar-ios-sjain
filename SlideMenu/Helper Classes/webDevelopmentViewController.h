//
//  webDevelopmentViewController.h
//  SlideMenu
//
//  Created by Nairitya Khilari on 12/12/14.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface webDevelopmentViewController : UITableViewController <SlideNavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate>
{
   NSArray *developmentServices;
}

- (NSArray *)webd;
@end