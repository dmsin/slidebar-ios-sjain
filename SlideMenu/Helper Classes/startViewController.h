//
//  startViewController.h
//  SlideMenu
//
//  Created by Nairitya Khilari on 11/12/14.
//

#import <UIKit/UIKit.h>

@interface startViewController : UIViewController{
    IBOutlet UIImageView *image;
}
@property (strong, nonatomic) IBOutlet UIButton *startButton;
- (IBAction)startAction:(id)sender;


@end
