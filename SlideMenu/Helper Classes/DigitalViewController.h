//
//  DigitalViewController.h
//  SlideMenu
//
//  Created by Nairitya Khilari on 12/12/14.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface DigitalViewController : UITableViewController <SlideNavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate>
{
    NSArray *digitalServices;
}

- (NSArray *)digital;
@end