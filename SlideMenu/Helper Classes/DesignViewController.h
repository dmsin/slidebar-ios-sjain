//
//  DesignViewController.h
//  SlideMenu
//
//  Created by Nairitya Khilari on 12/12/14.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface DesignViewController : UITableViewController <SlideNavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate>
{
    NSArray *designServices;
}
-(NSArray *)design;
@end