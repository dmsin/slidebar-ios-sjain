//
//  RightMenuViewController.m
//  SlideMenu
//
//  Created by Aryan Gh on 4/26/14.
//

#import "RightMenuViewController.h"
#import "DBMaintainer.h"
#import "MGSwipeButton.h"

#import "webDevelopmentSubViewController.h"
#import "WebHostingSubViewController.h"
#import "DigitalSubViewController.h"
#import "DesignSubViewController.h"
#import "ServicesViewController.h"

#define TEST_USE_MG_DELEGATE 1

@implementation RightMenuViewController{
    UIBarButtonItem *prevButton;
    UITableViewCellAccessoryType accessory;
}

#pragma mark - UIViewController Methods -
NSArray *allDbData;

-(void) cancelTableEditClick: (id) sender
{
    [_tableView setEditing: NO animated: YES];
    self.navigationItem.rightBarButtonItem = prevButton;
    prevButton = nil;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	self.tableView.separatorColor = [UIColor lightGrayColor];
    self.tableView.layoutMargins = UIEdgeInsetsMake(0,30, 0, 0);
    allDbData = [[DBMaintainer getSharedInstannce] findall];
    self.buttDone.enabled = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadtableData) name:@"refreshTable" object:nil];
}

#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    self.buttDone.enabled = (long)[allDbData count] == 0 ? NO : YES;
    return (long)[allDbData count]/2 + 3;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 20)];
	view.backgroundColor = [UIColor clearColor];
	return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MGSwipeTableCell * cell;
    static NSString * reuseIdentifier = @"rightMenuCell";
   
    cell = [[MGSwipeTableCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    
    if ([indexPath row] == (long)[allDbData count]/2){
        return cell;
    }
    else if([indexPath row] == (long)[allDbData count]/2 + 1){
        cell.textLabel.numberOfLines=5;
        NSString *rupee =@"\u20B9";
        
        NSString *gross = [[NSString alloc] initWithFormat:@"Gross Amount : %@%ld\n", rupee,(long)[self grossAmount]];
        UIFont *arialFont = [UIFont fontWithName:@"arial" size:16.0];
        NSDictionary *arialDict = [NSDictionary dictionaryWithObject: arialFont forKey:NSFontAttributeName];
        NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:gross attributes: arialDict];
        
        NSString *serviceTax = [[NSString alloc] initWithFormat:@"Service Tax (at 12%% of the gross Amount) : %@%ld\n", rupee, (long)(.12*[self grossAmount])];
        UIFont *VerdanaFont = [UIFont fontWithName:@"arial" size:12.0];
        NSDictionary *verdanaDict = [NSDictionary dictionaryWithObject:VerdanaFont forKey:NSFontAttributeName];
        NSMutableAttributedString *vAttrString = [[NSMutableAttributedString alloc]initWithString: serviceTax attributes:verdanaDict];
        [vAttrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:(NSMakeRange(0, 1))];
        [aAttrString appendAttributedString:vAttrString];
        
        NSString *education = [[NSString alloc] initWithFormat:@"Education Cess (at 2%% if ST) : %@%ld\n", rupee, (long)(.02*[self grossAmount])];
        UIFont *VerdanaFont1 = [UIFont fontWithName:@"arial" size:12.0];
        NSDictionary *verdanaDict1 = [NSDictionary dictionaryWithObject:VerdanaFont1 forKey:NSFontAttributeName];
        NSMutableAttributedString *vAttrString1 = [[NSMutableAttributedString alloc]initWithString: education attributes:verdanaDict1];
        [vAttrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:(NSMakeRange(0, 1))];
        [aAttrString appendAttributedString:vAttrString1];
        
        NSString *SHE = [[NSString alloc] initWithFormat:@"S.H.E. Cess (at 1%% of ST) : %@%ld\n\n", rupee, (long)(.01*[self grossAmount])];
        UIFont *VerdanaFont2 = [UIFont fontWithName:@"arial" size:12.0];
        NSDictionary *verdanaDict2 = [NSDictionary dictionaryWithObject:VerdanaFont2 forKey:NSFontAttributeName];
        NSMutableAttributedString *vAttrString2 = [[NSMutableAttributedString alloc]initWithString: SHE attributes:verdanaDict2];
        [vAttrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:(NSMakeRange(0, 1))];
        [aAttrString appendAttributedString:vAttrString2];
        
        NSString *net = [[NSString alloc] initWithFormat:@"Net Payable Amount : %@%ld\n", rupee, (long)(1.15*[self grossAmount])];
        UIFont *VerdanaFont3 = [UIFont fontWithName:@"arial" size:16.0];
        NSDictionary *verdanaDict3 = [NSDictionary dictionaryWithObject:VerdanaFont3 forKey:NSFontAttributeName];
        NSMutableAttributedString *vAttrString3 = [[NSMutableAttributedString alloc]initWithString: net attributes:verdanaDict3];
        [vAttrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:(NSMakeRange(0, 1))];
        [aAttrString appendAttributedString:vAttrString3];
        
        cell.textLabel.attributedText = aAttrString;
        
        return cell;
    }
    else if([indexPath row] == (long)[allDbData count]/2 + 2){
        return cell;
    }
    
    cell.textLabel.text = [allDbData objectAtIndex:[indexPath row]*2];
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    cell.accessoryType = accessory;
    cell.delegate = self;
    
    cell.backgroundColor = [UIColor clearColor];
    cell.selectedBackgroundView = [[UIView alloc] init];
    cell.selectedBackgroundView.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.8];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.swipeBackgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor colorWithRed:17/255 green:112/255 blue:1/255 alpha:1];
    cell.detailTextLabel.textColor = [UIColor redColor];
    
    cell.detailTextLabel.text = [[NSString alloc] initWithFormat:@"INR %@", [allDbData objectAtIndex:[indexPath row]*2 + 1]];
    return cell;
}

#if TEST_USE_MG_DELEGATE
-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings;
{
    NSArray *butt;
    if (direction == MGSwipeDirectionLeftToRight) {
        expansionSettings.buttonIndex = 1;
        expansionSettings.fillOnTrigger = NO;
        return butt;
    }
    else {
        expansionSettings.buttonIndex = 1;
        expansionSettings.fillOnTrigger = NO;
        butt = [self createRightButtons:2];
        return butt;
    }
}
#endif

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == (long)[allDbData count]/2 + 1){
        return 100;
    }
    else
    {
     return 40;
    }
}

-(NSArray *) createRightButtons: (int) number
{
    NSMutableArray * result = [NSMutableArray array];
    NSString* titles[2] = {@"Delete", @"More"};
    UIColor * colors[2] = {[UIColor redColor], [UIColor lightGrayColor]};
    
    for (int i = 0; i < number; ++i)
    {
        MGSwipeButton * button = [MGSwipeButton buttonWithTitle:titles[i] backgroundColor:colors[i] callback:^BOOL(MGSwipeTableCell * sender){
            NSLog(@"Convenience callback received (right).");
            return YES;
        }];
        [result addObject:button];
    }
    return result;
}

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    NSLog(@"Delegate: button tapped, %@ position, index %d, from Expansion: %@",direction == MGSwipeDirectionLeftToRight ? @"left" : @"right", (int)index, fromExpansion ? @"YES" : @"NO");
    
    NSIndexPath *path = [_tableView indexPathForCell:cell];
    
    if (direction == MGSwipeDirectionRightToLeft && index == 0) {
        bool del = [[DBMaintainer getSharedInstannce] deleteAnEntry:[allDbData objectAtIndex:(long)path.row*2]];
        [self reloadtableData];
        return del;
    }
    
    return YES;
}


-(void) tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (IBAction)actDone:(id)sender {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone"
                                                             bundle: nil];
    
    UIViewController *vc ;
    
    vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"userInfoViewController"];
    [[SlideNavigationController sharedInstance] pushViewController:vc animated:YES];
    
}

- (void)reloadtableData
{
    allDbData = [[DBMaintainer getSharedInstannce] findall];
    [self.tableView reloadData];
}

- (void)reloadalltableData
{
    allDbData = [[DBMaintainer getSharedInstannce] findall];
    [self.tableView reloadData];
    [[webDevelopmentSubViewController alloc]reloadtableData];
    [[WebHostingSubViewController alloc]reloadtableData];
    [[DesignSubViewController alloc] reloadtableData];
    [[DigitalSubViewController alloc]reloadtableData];
}

- (NSInteger) grossAmount
{
    NSInteger gross = 0;
    int p;
    for(int i=0; i<[allDbData count]/2; i++){
        
        p = [[allDbData objectAtIndex:i*2 + 1] intValue];
        //NSLog(@"%c", p);
        gross += p;
    }
    return gross;
}
@end
