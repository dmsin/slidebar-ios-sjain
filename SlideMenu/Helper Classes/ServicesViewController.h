//
//  ServicesViewController.h
//  SlideMenu
//
//  Created by Nairitya Khilari on 12/12/14.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface ServicesViewController : UITableViewController <SlideNavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate>
{
    NSArray *serviceServices;
}
- (NSArray *)service;

@property (nonatomic, assign) BOOL slideOutAnimationEnabled;
@end