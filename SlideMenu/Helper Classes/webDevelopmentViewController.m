//
//  webDevelopmentViewController.m
//  SlideMenu
//
//  Created by Nairitya Khilari on 12/12/14.
//


#import "webDevelopmentViewController.h"

@implementation webDevelopmentViewController

//Send this to child
NSString *webDevelopmentServiceChoosen;

- (void)viewDidLoad
{
    [super viewDidLoad];
    developmentServices = [self webd];
    UIButton *cartButton  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [cartButton setImage:[UIImage imageNamed:@"cart"] forState:UIControlStateNormal];
    [cartButton addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleRightMenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cartBarButton = [[UIBarButtonItem alloc] initWithCustomView:cartButton ];
    self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects:cartBarButton, nil];
    UIImageView *imageLogoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DMS LOGO.png"]];
    imageLogoView.frame = CGRectMake(130, 80, 60, 30);
    self.navigationItem.titleView = imageLogoView;
    
    UIButton *searchButton  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [searchButton setImage:[UIImage imageNamed:@"search.gif"] forState:UIControlStateNormal];
    [searchButton addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *searchBarButton = [[UIBarButtonItem alloc] initWithCustomView:searchButton ];
    self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects:cartBarButton, searchBarButton, nil];
}

-(void)searchAction
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle: nil];
    UIViewController *vc ;
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"rohan"];
    [[SlideNavigationController sharedInstance] pushViewController:vc animated:NO];
    NSLog(@"search button clicked");
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"friendCell"];
    
    cell.textLabel.text = [developmentServices objectAtIndex:[indexPath row]];
    cell.textLabel.textColor = [UIColor colorWithRed:17/255 green:112/255 blue:1/255 alpha:1];
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    webDevelopmentServiceChoosen = [developmentServices objectAtIndex:[indexPath row]];
    
    [[NSUserDefaults standardUserDefaults] setObject:webDevelopmentServiceChoosen forKey:@"webDevelopmentServiceChoosen"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSArray *)webd
{
    return [[NSArray alloc]initWithObjects: @"Website Development", @"Website Redesigning", @"E-Commerce", nil];
}
@end
