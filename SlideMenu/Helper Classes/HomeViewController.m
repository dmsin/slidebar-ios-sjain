//
//  HomeViewController.m
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Aryan Ghassemi. All rights reserved.
//

#import "HomeViewController.h"
#import "LeftMenuViewController.h"
#import "CycleScrollView.h"
#import "SearchViewController.h"

@implementation HomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, 0);
    
    UIImageView *imageLogoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DMS LOGO.png"]];
    imageLogoView.frame = CGRectMake(130, 80, 60, 30);
    self.navigationItem.titleView = imageLogoView;
    
    // Do any additional setup after loading the view, typically from a nib.
    NSMutableArray *picArray = [[NSMutableArray alloc] init];
    [picArray addObject:[UIImage imageNamed:@"slider1.jpg"]];
    [picArray addObject:[UIImage imageNamed:@"slider2.jpg"]];
    [picArray addObject:[UIImage imageNamed:@"slider3.jpg"]];
    [picArray addObject:[UIImage imageNamed:@"slider4.jpg"]];
    
    CycleScrollView *cycle = [[CycleScrollView alloc] initWithFrame:CGRectMake(0, 20, 320, 370)
                                                     cycleDirection:CycleDirectionLandscape
                                                           pictures:picArray];
    cycle.delegate = self;
    [self.view addSubview:cycle];
    
    UIButton *cartButton  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [cartButton setImage:[UIImage imageNamed:@"cart.png"] forState:UIControlStateNormal];
    [cartButton addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleRightMenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cartBarButton = [[UIBarButtonItem alloc] initWithCustomView:cartButton ];
    
    UIButton *searchButton  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [searchButton setImage:[UIImage imageNamed:@"search.gif"] forState:UIControlStateNormal];
    [searchButton addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *searchBarButton = [[UIBarButtonItem alloc] initWithCustomView:searchButton ];
    
    self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects:cartBarButton, searchBarButton, nil];
    
   
}

-(void)searchAction
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle: nil];
    UIViewController *vc ;
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"rohan"];
    [[SlideNavigationController sharedInstance] pushViewController:vc animated:NO];
    NSLog(@"search button clicked");
}

- (void)cycleScrollViewDelegate:(CycleScrollView *)cycleScrollView didSelectImageView:(int)index {
    
}

- (void)cycleScrollViewDelegate:(CycleScrollView *)cycleScrollView didScrollImageView:(int)index {
    
    self.title = [NSString stringWithFormat:@"DMS"];
}
#pragma mark CycleScrollViewDelegate End -

- (void)dealloc
{
}


#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
	return YES;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
	return YES;
}
- (IBAction)Faecbaouk:(UIButton *)sender
{
    [[NSUserDefaults standardUserDefaults] setObject:@"https://www.facebook.com/DMSInfoSystem" forKey:@"socialLinkKey"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)Taueiter:(UIButton *)sender
{
    [[NSUserDefaults standardUserDefaults] setObject:@"https://twitter.com/DMSInfosystem" forKey:@"socialLinkKey"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)GogliPaelus:(UIButton *)sender
{
    [[NSUserDefaults standardUserDefaults] setObject:@"https://plus.google.com/107061259898707893000/posts" forKey:@"socialLinkKey"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)Lainkdiin:(UIButton *)sender
{
    [[NSUserDefaults standardUserDefaults] setObject:@"https://www.linkedin.com/company/2354729?trk=tyah&trkInfo=tarId%3A1419311456851%2Ctas%3Adms%20infosystem%2Cidx%3A2-1-2" forKey:@"socialLinkKey"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)ContactAss:(UIButton *)sender
{
    [[NSUserDefaults standardUserDefaults] setObject:@"http://dmsinfosystem.com/contact/" forKey:@"socialLinkKey"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
@end
